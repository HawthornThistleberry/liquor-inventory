VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Bottle Inventory Management For Bar"
   ClientHeight    =   7755
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10200
   Icon            =   "frmMain.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   500
   ScaleMode       =   0  'User
   ScaleWidth      =   700
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdEditProductList 
      Caption         =   "Edit Product List"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   2760
      Width           =   2415
   End
   Begin VB.Frame fraReports 
      Caption         =   "Reports"
      Height          =   2295
      Left            =   0
      TabIndex        =   20
      Top             =   3360
      Width           =   2655
      Begin VB.CommandButton cmdGenerateReport 
         Caption         =   "Generate Report"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   1800
         Width           =   2415
      End
      Begin VB.ListBox lisReports 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1500
         ItemData        =   "frmMain.frx":0442
         Left            =   120
         List            =   "frmMain.frx":0458
         TabIndex        =   6
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.Frame fraOtherFunctions 
      Caption         =   "Other Functions"
      Height          =   1695
      Left            =   0
      TabIndex        =   19
      Top             =   6000
      Width           =   2655
      Begin VB.CommandButton cmdAbout 
         Caption         =   "About This Program"
         Height          =   375
         Left            =   120
         TabIndex        =   10
         Top             =   1200
         Width           =   2415
      End
      Begin VB.CommandButton cmdEndDay 
         Caption         =   "End Day"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   720
         Width           =   2415
      End
      Begin VB.CommandButton cmdBackup 
         Caption         =   "Back Up Data"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.Frame fraDailyDisplay 
      Caption         =   "Daily Transfers"
      Height          =   7695
      Left            =   2760
      TabIndex        =   17
      Top             =   0
      Width           =   7335
      Begin RichTextLib.RichTextBox rtbTotalsByCode 
         Height          =   6855
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   12091
         _Version        =   393217
         ScrollBars      =   2
         TextRTF         =   $"frmMain.frx":04B6
      End
      Begin VB.ListBox lisDailyLog 
         Height          =   6885
         Left            =   120
         TabIndex        =   24
         Top             =   240
         Width           =   7095
      End
      Begin VB.Timer tmrClock 
         Interval        =   300
         Left            =   4680
         Top             =   7200
      End
      Begin VB.OptionButton optDataDisplayType 
         Caption         =   "Totals By Product"
         Height          =   255
         Index           =   0
         Left            =   840
         TabIndex        =   12
         Top             =   7320
         Value           =   -1  'True
         Width           =   1575
      End
      Begin VB.OptionButton optDataDisplayType 
         Caption         =   "Daily Log (allows editing)"
         Height          =   255
         Index           =   1
         Left            =   2520
         TabIndex        =   13
         Top             =   7320
         Width           =   2175
      End
      Begin VB.Label lblClock 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   5160
         TabIndex        =   23
         Top             =   7320
         Width           =   2055
      End
      Begin VB.Label lblDataDisplayOptions 
         Caption         =   "Display:"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   7320
         Width           =   615
      End
   End
   Begin VB.Frame fraDataEntry 
      Caption         =   "Transfer Inventory"
      Height          =   2415
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   2655
      Begin VB.CommandButton cmdEnterTransfer 
         Caption         =   "Enter Transfer"
         Default         =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   1920
         Width           =   2415
      End
      Begin VB.OptionButton optQuantityType 
         Caption         =   "packs"
         Height          =   255
         Index           =   1
         Left            =   1200
         TabIndex        =   3
         Top             =   1560
         Width           =   1335
      End
      Begin VB.OptionButton optQuantityType 
         Caption         =   "bottles"
         Height          =   255
         Index           =   0
         Left            =   1200
         TabIndex        =   2
         Top             =   1320
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.TextBox txtQuantity 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         MaxLength       =   8
         TabIndex        =   1
         Text            =   "Quantity"
         Top             =   1320
         Width           =   975
      End
      Begin VB.TextBox txtUPC 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   120
         MaxLength       =   16
         TabIndex        =   0
         Top             =   240
         Width           =   2415
      End
      Begin VB.Label lblCategory 
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   1680
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label lblProductCode 
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   960
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label lblPackNumber 
         Height          =   255
         Left            =   1440
         TabIndex        =   21
         Top             =   960
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblPrice 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   960
         Width           =   2415
      End
      Begin VB.Label lblProductDescription 
         Caption         =   "Enter or scan a UPC above."
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   720
         Width           =   2415
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Bottle Inventory Management For Bar
' Written by Frank J. Perricone (HunterGreen) for afrikandesi
' for Rent-A-Coder job #319065

' Popping up a form within LostFocus events causes the field to lose focus and
' thus causes the LostFocus event to fire again!  How dumb is that?  We use
' a global variable to work around this so that the second time nothing happens.
Dim AlreadyLosingFocus As Boolean

Dim BackupLocation As String

'-----------------------------------------------------------------------------
' Form Handling --------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub Form_Load()
  On Error Resume Next
  ' position the window where it was last, or if it was never positioned before, in the middle
  Left = GetSetting(conAppName, "Startup", "Left", (Screen.Width - Width) / 2)
  Top = GetSetting(conAppName, "Startup", "Top", (Screen.Height - Height) / 2)
  BackupLocation = GetSetting(conAppName, "Startup", "BackupLocation", "")
  ' open the databases
  frmMain.Caption = "Opening databases, please wait..."
  frmMain.MousePointer = vbHourglass
  If Not OpenDatabases() Then
      Me.Tag = "unloading"
      End
    End If
  ' display the right details pane
  frmMain.Caption = "Setting up display, please wait..."
  If GetSetting(conAppName, "Startup", "DailyDisplay", 1) = 1 Then
      optDataDisplayType(0) = True
    Else
      optDataDisplayType(1) = True
    End If
  optDataDisplayType_Click GetSetting(conAppName, "Startup", "DailyDisplay", 1) - 1
  ' set the form name so we can change the program name in one place
  Form_DefaultTitle
  ' initialize a few state things
  AlreadyLosingFocus = False
  frmProductListEditor.Tag = ""
  Me.Tag = ""
  ' start with the UPC field focused
  frmMain.MousePointer = vbDefault
  txtUPC.SetFocus
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Dim i As Integer
  On Error GoTo ErrorHandler
  If Me.Tag <> "unloading" Then
      i = MsgBox("Would you like to run End Day before closing?", vbQuestion + vbYesNoCancel)
      If i = vbCancel Then
          Cancel = True
          Exit Sub
        End If
      If i = vbYes Then
          cmdEndDay_Click
          Exit Sub
        End If
    End If
  ' close the databases
  CloseDatabases
  ' unload the other forms
  Unload frmProductListEditor
  Unload frmUnknownUPC
  Unload frmEditTransfer
  Unload frmPrintPreview
  Unload frmReportParameters
  ' save settings
  Call SaveSetting(conAppName, "Startup", "Left", Left)
  Call SaveSetting(conAppName, "Startup", "Top", Top)
  Call SaveSetting(conAppName, "Startup", "BackupLocation", BackupLocation)
  If optDataDisplayType(0) Then Call SaveSetting(conAppName, "Startup", "DailyDisplay", 1) Else Call SaveSetting(conAppName, "Startup", "DailyDisplay", 2)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:Form_Unload", "") <> vbYes Then End
End Sub

' Sets a default title on the main window; created to centralize the code.
Private Sub Form_DefaultTitle()
  On Error GoTo ErrorHandler
  Me.Caption = conAppName & " (v" & CStr(App.Major) & "." & CStr(App.Minor) & "." & CStr(App.Revision) & ")"
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:Form_DefaultTitle", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Edit Product List ----------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub cmdEditProductList_Click()
  ' All of this is handled in the frmProductListEditor form and code
  On Error GoTo ErrorHandler
  frmProductListEditor.Tag = ""
  frmProductListEditor.Show 1
  UpdateDailyDisplay Now, ""
  ' Always return focus to the UPC field after everything
  txtUPC.SetFocus
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:cmbEditProductList_Click", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Clock ----------------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub tmrClock_Timer()
  ' Helps the user be aware if the system time is off
  On Error GoTo ErrorHandler
  lblClock = Format(Now, "hh:mm:ssampm - mmm d yyyy")
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:tmrClock_Timer", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Data Entry -----------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub txtUPC_GotFocus()
  On Error GoTo ErrorHandler
  SelectField txtUPC
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:txtUPC_GotFocus", "") <> vbYes Then End
End Sub

Private Sub txtUPC_KeyPress(KeyAscii As Integer)
  On Error GoTo ErrorHandler
  If KeyAscii = 13 Or KeyAscii = 10 Then
      ' since the barcode wedge scanner ends codes with Enter, handle it specially
      KeyAscii = 0
      txtUPC_LostFocus ' force it to accept this as an entry completion
      Exit Sub
    End If
  If InStr("0123456789", Chr$(KeyAscii)) = 0 And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      KeyAscii = 0
      Beep
    End If
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:txtUPC_KeyPress", "") <> vbYes Then End
End Sub

Private Sub txtUPC_LostFocus()
  Dim UPC As String
  Dim ProdCode As Integer
  Dim ProdInfo As ProductInfo
  On Error GoTo ErrorHandler
  If AlreadyLosingFocus Then Exit Sub
  AlreadyLosingFocus = True
  ' look up the relevant product information
  UPC = PrepareUPC(txtUPC)
  If UPC = "" Or UPC = "0000000000000000" Then
      txtUPC = ""
      AlreadyLosingFocus = False
      Exit Sub
    End If
  ProdCode = UPCLookup(UPC)
  If ProdCode = 0 Then
      ' handle the special case of an unlisted UPC
      frmUnknownUPC.lblUPC = UPC
      frmUnknownUPC.Tag = ""
      frmUnknownUPC.Show 1
      ' Now do it!
      If Left$(frmUnknownUPC.Tag, 4) = "link" Then
          ' link the UPC to the product code
          ProdCode = Val(Mid$(frmUnknownUPC.Tag, 6))
          ' do the link
          If LinkUPCToProductCode(UPC, ProdCode) = 0 Then
              MsgBox "Unable to link the UPC to that product!", vbExclamation
              txtUPC = ""
              txtUPC.SetFocus
              AlreadyLosingFocus = False
              Exit Sub
            End If
          ' from here, we just drop through
          ' the rest of this routine will proceed as if this link already existed
        ElseIf Left$(frmUnknownUPC.Tag, 6) = "create" Then
          ' launch the product editor having it start the creation
          frmProductListEditor.Tag = CStr(frmUnknownUPC.cmbCategory.ListIndex + 1) & " " & UPC
          frmProductListEditor.Show 1
          ' when it returns, put us back here to try again
          txtUPC.SetFocus
          AlreadyLosingFocus = False
          Exit Sub
        Else ' assume cancel
          txtUPC = ""
          txtUPC.SetFocus
          AlreadyLosingFocus = False
          Exit Sub
        End If
    End If
  If Not ProductLookup(ProdCode, ProdInfo) Then
      MsgBox "Unable to look up information on that product!", vbExclamation
      txtUPC = ""
      txtUPC.SetFocus
      AlreadyLosingFocus = False
      Exit Sub
    End If
  ' fill in the appropriate fields
  cmdEnterTransfer.Enabled = True
  With ProdInfo
    lblProductDescription = .Name
    lblPrice = "Price: $" & Format(.Price, "#,##0.00") & " each"
    txtQuantity = CStr(.DefaultQuantity)
    lblPackNumber = CStr(.PackNumber)   ' this is a hidden field used as a global variable
    lblProductCode = CStr(.ProductCode) ' this too
    lblCategory = CStr(.Category)       ' and this, boy am I naughty!
    If .Category = conProdCat_Liquor Then
        optQuantityType(0).Visible = False
        optQuantityType(1).Visible = False
        optQuantityType(0) = True
      Else
        optQuantityType(0).Visible = True
        optQuantityType(1).Visible = True
        optQuantityType(1).Caption = .PackName & "(s)"
        If .DefaultByPack Then optQuantityType(1) = True Else optQuantityType(0) = True
      End If
  End With
  ' move the focus to Enter Transfer
  cmdEnterTransfer.SetFocus
  AlreadyLosingFocus = False
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:txtUPC_LostFocus", "") <> vbYes Then End
End Sub

Private Sub txtQuantity_GotFocus()
  On Error GoTo ErrorHandler
  SelectField txtQuantity
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:txtQuantity_GotFocus", "") <> vbYes Then End
End Sub

Private Sub txtQuantity_KeyPress(KeyAscii As Integer)
  On Error GoTo ErrorHandler
  KeyAscii = KP_OnlyDigits(KeyAscii)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:txtQuantity_KeyPress", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Entering Transfers ---------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub cmdEnterTransfer_Click()
  Dim Xfer As Transfer
  ' create the transfer
  On Error GoTo ErrorHandler
  Xfer.DateTime = Now
  Xfer.ProductCode = Val(lblProductCode)
  Xfer.Price = CCur(Mid$(lblPrice, 9, Len(lblPrice) - 13))
  Xfer.Quantity = Val(txtQuantity) * Val(lblPackNumber)
  Xfer.Notes = ""
  If Not CreateTransfer(Xfer) Then
      MsgBox "Unable to store transfer!", vbExclamation
    Else
      ' update the daily display
      UpdateDailyDisplay Xfer.DateTime, AddItemToDailyLog(Xfer.DateTime, lblProductDescription, lblCategory, Xfer.Quantity, Xfer.Price, Xfer.Notes)
    End If
  ' clear the other fields to prepare for the next data entry
  txtUPC = ""
  lblProductDescription = "Enter or scan a UPC above."
  lblPrice = ""
  lblPackNumber = ""
  lblProductCode = ""
  txtQuantity = "Quantity"
  optQuantityType(0).Visible = True
  optQuantityType(1).Visible = True
  optQuantityType(1).Caption = "packs"
  ' move the focus back to the UPC field
  txtUPC.SetFocus
  cmdEnterTransfer.Enabled = False
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:cmdEnterTransfer_Click", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Daily Display --------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub optDataDisplayType_Click(Index As Integer)
  On Error GoTo ErrorHandler
  If optDataDisplayType(0) Then
      rtbTotalsByCode.Visible = True
      lisDailyLog.Visible = False
    Else
      rtbTotalsByCode.Visible = False
      lisDailyLog.Visible = True
    End If
  UpdateDailyDisplay Now, ""
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:optDataDisplayType_Click", "") <> vbYes Then End
End Sub

' Handles all updates to the daily display, whichever one it is.  If you pass in a transfer's details, it'll just add that to the list.
Private Sub UpdateDailyDisplay(ByVal DateTime As Date, ByVal TransferDesc As String)
  Dim PointerMode As Boolean
  Dim i As Integer, Duplicate As Boolean
  On Error GoTo ErrorHandler
  If frmMain.MousePointer = vbDefault Then
      PointerMode = True
      frmMain.MousePointer = vbHourglass
    Else
      PointerMode = False
    End If
  If rtbTotalsByCode.Visible Or Not Me.Visible Then
      ' generate the TotalsByCode display
      rtbTotalsByCode = TotalsByCode(Now, Now, "Daily Totals By Code")
    End If
  If lisDailyLog.ListCount = 0 Then
      RebuildDailyLog
    End If
  If TransferDesc <> "" Then
      ' append the passed-in entries to it, only if it's today and that one isn't already listed
      If Format(DateTime, "yyyy-mm-dd") = Format(Now, "yyyy-mm-dd") Then
          Duplicate = False
          For i = 0 To lisDailyLog.ListCount - 1
            ' extract date
            If ExtractDatestampFromDailyLog(lisDailyLog.List(i)) = DateTime Then Duplicate = True
            Next
          If Not Duplicate Then
              lisDailyLog.AddItem TransferDesc
            End If
        End If
    End If
  If PointerMode Then frmMain.MousePointer = vbDefault
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:UpdateDailyDisplay", "") <> vbYes Then End
End Sub

' Generates the totals by code display text
Private Function TotalsByCode(ByVal StartDate As Date, ByVal EndDate As Date, ByVal Title As String) As String
  Dim adoSortedTransfers As New ADODB.Recordset
  Dim Result As String, CategoryList(conProdCat_Other) As String
  Dim Category As Integer
  Dim ProdInfo As ProductInfo
  Dim LastProductCode As Integer, LastProductCategory As Integer, LastProductName As String, LastProductBottles As Integer, LastProductDollars As Currency
  Dim TotalBottles(conProdCat_Other) As Integer, TotalDollars(conProdCat_Other) As Currency
  Dim GrandTotalBottles As Integer, GrandTotalDollars As Currency
  
  ' All reports are date ranges, not time ranges, so normalize the date to be the beginning of StartDate and the end of EndDate (actually the beginning of the following date)
  StartDate = CDate(Format(StartDate, "mm-dd-yyyy"))
  EndDate = CDate(Format(EndDate, "mm-dd-yyyy")) + 1
  
  ' First, open a recordset that has the transfers sorted by product code.
  ' We do this so we can subtotal by product.
  Set adoSortedTransfers = New ADODB.Recordset
  On Error Resume Next
  Err.Clear
  adoSortedTransfers.Open "Select * From Transfers Order By ProductCode", adoTransfersDatabase, , , adCmdText
  If Err <> 0 Then
    TotalsByCode = BuildRTBString(2, "\b Error: unable to query transfers to build totals by code!\plain\f2\fs17\par ")
    Exit Function
  End If
  
  ' Start with zeroes in everything
  GrandTotalBottles = 0
  GrandTotalDollars = 0
  Result = ""
  For Category = conProdCat_Liquor To conProdCat_Other
    TotalBottles(Category) = 0
    TotalDollars(Category) = 0
    CategoryList(Category) = ""
    Next
  
  ' Loop through all transfers
  adoSortedTransfers.MoveFirst
  LastProductCode = 0
  Do Until adoSortedTransfers.EOF
    ' Only pay attention to ones dated today
    If adoSortedTransfers!DateTime >= StartDate And adoSortedTransfers!DateTime <= EndDate Then
       ' Only pay attention to ones for which we can load product information
        If ProductLookup(adoSortedTransfers!ProductCode, ProdInfo) Then
            ' This requires some explanation.  We need to read all the Absolut Vodka records
            ' before producing any output, but we won't know which is the last Absolut Vodka
            ' record until we hit the first record after them.  So we need to keep a running
            ' total for each code, and only dump it just after it changes.  To do this, we
            ' keep checking if this is a new code, and thus, time to dump the last one.
            If ProdInfo.ProductCode <> LastProductCode Then
                ' this is a new product code, time to dump and clear
                If LastProductCode <> 0 Then
                    ' The check above prevents us dumping the nothing we had before we first came in.
                    TotalBottles(LastProductCategory) = TotalBottles(LastProductCategory) + LastProductBottles
                    TotalDollars(LastProductCategory) = TotalDollars(LastProductCategory) + LastProductDollars
                    CategoryList(LastProductCategory) = CategoryList(LastProductCategory) & "    " & LastProductName
                    CategoryList(LastProductCategory) = CategoryList(LastProductCategory) & "\tab " & CStr(LastProductBottles)
                    CategoryList(LastProductCategory) = CategoryList(LastProductCategory) & "\tab " & Format(LastProductDollars, "#,##0.00") & "\par "
                  End If
                ' Now that that's dumped, we can load up a new one.
                LastProductCode = ProdInfo.ProductCode
                LastProductName = ProdInfo.Name
                LastProductCategory = ProdInfo.Category
                LastProductBottles = adoSortedTransfers!Quantity
                LastProductDollars = adoSortedTransfers!Price * adoSortedTransfers!Quantity
              Else
                ' This code is the same as the last one, so just accumulate it.
                LastProductBottles = LastProductBottles + adoSortedTransfers!Quantity
                LastProductDollars = LastProductDollars + adoSortedTransfers!Price * adoSortedTransfers!Quantity
              End If
          End If
      End If
    adoSortedTransfers.MoveNext
  Loop
  adoSortedTransfers.Close
  ' Now we need to dump the last one that never got dumped because we hit the end
  TotalBottles(LastProductCategory) = TotalBottles(LastProductCategory) + LastProductBottles
  TotalDollars(LastProductCategory) = TotalDollars(LastProductCategory) + LastProductDollars
  CategoryList(LastProductCategory) = CategoryList(LastProductCategory) & "    " & LastProductName
  CategoryList(LastProductCategory) = CategoryList(LastProductCategory) & "\tab " & CStr(LastProductBottles)
  CategoryList(LastProductCategory) = CategoryList(LastProductCategory) & "\tab " & Format(LastProductDollars, "#,##0.00") & "\par "
  
  ' We've finished building all the product information.  Now, let's put it all together.
  ' First, we'll generate the block for each category.
  For Category = conProdCat_Liquor To conProdCat_Other
    If TotalBottles(Category) <> 0 Then ' This category does need to be on the report
        Result = Result & "\b " & CategoryName(Category) & "\tab Qty\tab Dollars\plain\f2\fs17\par "
        Result = Result & CategoryList(Category)
        Result = Result & "\b " & CategoryName(Category) & " Totals"
        Result = Result & "\tab " & CStr(TotalBottles(Category))
        Result = Result & "\tab " & Format(TotalDollars(Category), "#,##0.00")
        Result = Result & "\plain\f2\fs17\par\par "
        ' Add up grand totals as we go.
        GrandTotalBottles = GrandTotalBottles + TotalBottles(Category)
        GrandTotalDollars = GrandTotalDollars + TotalDollars(Category)
      End If
    Next

  If GrandTotalBottles <> 0 Then
      ' Add in a grand totals section
      Result = Result & "\b Grand Totals\tab " & CStr(GrandTotalBottles)
      Result = Result & "\tab " & Format(GrandTotalDollars, "#,##0.00")
      Result = Result & "\plain\f2\fs17\par\par "
    Else
      ' This can only happen if there wasn't anything at all!
      Result = "\b No transfers on file for selected date(s).\plain\f2\fs17\par "
    End If

  Result = "\b " & Title & " for " & Format(StartDate, "dd-mmm-yyyy") & IIf(StartDate <> EndDate - 1, " to " & Format(EndDate - 1, "dd-mmm-yyyy"), "") & "\plain\f2\fs17\par\par " & Result
  
  TotalsByCode = BuildRTBString(2, Result)
End Function

' Builds a daily log when the program starts up or has lost track.  Normally it'll be built as we go from there.
Private Sub RebuildDailyLog()
  Dim adoSortedTransfers As New ADODB.Recordset
  Dim ProdInfo As ProductInfo
  
  ' First, open a recordset that has the transfers sorted by datestamp.
  Set adoSortedTransfers = New ADODB.Recordset
  On Error Resume Next
  Err.Clear
  lisDailyLog.Clear
  adoSortedTransfers.Open "Select * From Transfers Order By DateTime", adoTransfersDatabase, , , adCmdText
  If Err <> 0 Then
    lisDailyLog.AddItem "Error: unable to query transfers to build daily log!"
    Exit Sub
  End If
  
  ' Loop through all transfers
  adoSortedTransfers.MoveFirst
  Do Until adoSortedTransfers.EOF
    ' Only pay attention to ones dated today
    If Format(adoSortedTransfers!DateTime, "yyyy-mm-dd") = Format(Now, "yyyy-mm-dd") Then
       ' Only pay attention to ones for which we can load product information
        If ProductLookup(adoSortedTransfers!ProductCode, ProdInfo) Then
            lisDailyLog.AddItem AddItemToDailyLog(adoSortedTransfers!DateTime, ProdInfo.Name, ProdInfo.Category, adoSortedTransfers!Quantity, adoSortedTransfers!Price, adoSortedTransfers!Notes)
          End If
      End If
    adoSortedTransfers.MoveNext
  Loop
  adoSortedTransfers.Close
End Sub

' Builds items for the daily log.
Public Function AddItemToDailyLog(ByVal DateTime As Date, ByVal Name As String, ByVal Category As Integer, _
                              ByVal Quantity As Integer, ByVal Price As Currency, ByVal Notes As String)
  Dim Result As String
  On Error GoTo ErrorHandler
  Result = Format(DateTime, "mm-dd-yyyy hh:mm:ssampm") & ": " & Name & " (" & CategoryName(Category)
  Result = Result & ") - " & CStr(Quantity) & " @ " & Format(Price, "#,##0.00") & " = $"
  Result = Result & Format(Quantity * Price, "#,##0.00")
  If Notes <> "" Then Result = Result & " [Note]"
  AddItemToDailyLog = Result
  Exit Function
ErrorHandler:
  If ReportRuntimeError("frmMain:AddItemToDailyLog", "") <> vbYes Then End
End Function

' Give a log entry created by the above function, converts the datestamp text back into a Date.
Private Function ExtractDatestampFromDailyLog(ByVal s As String) As Date
  On Error GoTo ErrorHandler
  If Mid$(s, 3, 1) <> "-" Then ExtractDatestampFromDailyLog = 0 Else ExtractDatestampFromDailyLog = CDate(Left$(s, 21))
  Exit Function
ErrorHandler:
  If ReportRuntimeError("frmMain:ExtractDatestampFromDailyLog", "") <> vbYes Then End
End Function

' Double-clicking the daily log allows you to edit that entry
Private Sub lisDailyLog_DblClick()
  Dim Xfer As Transfer
  Dim ProdInfo As ProductInfo
  On Error GoTo ErrorHandler
  ' fire up the other form that handles this, passing in the DateTime
  frmEditTransfer.DateTime = ExtractDatestampFromDailyLog(lisDailyLog.List(lisDailyLog.ListIndex))
  If frmEditTransfer.DateTime = 0 Then Exit Sub ' this could be the "nothing here" error message line
  frmEditTransfer.Show 1
  If frmEditTransfer.Tag = "ok" Then
      ' update the relevant list item
      If Not TransferLookup(frmEditTransfer.DateTime, Xfer) Then
          RebuildDailyLog
          Exit Sub
        End If
      If Not ProductLookup(Xfer.ProductCode, ProdInfo) Then
          RebuildDailyLog
          Exit Sub
        End If
      lisDailyLog.List(lisDailyLog.ListIndex) = AddItemToDailyLog(frmEditTransfer.DateTime, ProdInfo.Name, ProdInfo.Category, Xfer.Quantity, Xfer.Price, Xfer.Notes)
    End If
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:lisDailyLog_DblClick", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Reports --------------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub lisReports_Click()
  On Error GoTo ErrorHandler
  If lisReports.ListIndex >= 0 Then
      cmdGenerateReport.Enabled = True
      cmdGenerateReport.SetFocus
    Else
      cmdGenerateReport.Enabled = False
    End If
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:lisReports_Click", "") <> vbYes Then End
End Sub

Private Function ReportParameters(EndDate As Boolean, Products As Boolean) As Boolean
  On Error GoTo ErrorHandler
  If EndDate Then
      frmReportParameters.lblDates = "Dates:"
      frmReportParameters.lblTo.Visible = True
      frmReportParameters.dtpEndDate.Visible = True
    Else
      frmReportParameters.lblDates = "Date:"
      frmReportParameters.lblTo.Visible = False
      frmReportParameters.dtpEndDate.Visible = False
    End If
  frmReportParameters.lblProduct.Visible = Products
  frmReportParameters.cmbProduct.Visible = Products
  frmReportParameters.Show 1
  If frmReportParameters.Tag = "cancel" Then ReportParameters = False Else ReportParameters = True
  Exit Function
ErrorHandler:
  If ReportRuntimeError("frmMain:ReportParameters", "") <> vbYes Then End
End Function

Private Sub cmdGenerateReport_Click()
  Dim ReportBody As String, ReportTitle As String
  On Error GoTo ErrorHandler
  Select Case lisReports.ListIndex
    Case 0 ' Daily Totals Report
      ReportBody = TotalsByCode(Now, Now, "Daily Totals Report")
      ReportTitle = "Daily Totals Report - " & Format(Now, "dd-mmm-yyyy")
    Case 1 ' Single Date Report
      If Not ReportParameters(False, False) Then Exit Sub
      ReportBody = TotalsByCode(CDate(frmReportParameters.dtpStartDate), CDate(frmReportParameters.dtpStartDate), "Single Date Report")
      ReportTitle = "Single Date Report - " & Format(CDate(frmReportParameters.dtpStartDate), "dd-mmm-yyyy")
    Case 2 ' Month To Date Report
      ReportBody = TotalsByCode(Now - Day(Now) + 1, Now, "Month-To-Date Report")
      ReportTitle = "Month To Date Report - " & Format(Now, "dd-mmm-yyyy")
    Case 3 ' Year To Date Report
      ReportBody = TotalsByCode(Now - CInt(Format(Now, "y")) + 1, Now, "Year-To-Date Report")
      ReportTitle = "Year To Date Report - " & Format(Now, "dd-mmm-yyyy")
    Case 4 ' Custom Dates Report
      If Not ReportParameters(True, False) Then Exit Sub
      ReportBody = TotalsByCode(CDate(frmReportParameters.dtpStartDate), CDate(frmReportParameters.dtpEndDate), "Custom Dates Report")
      ReportTitle = "Custom Dates Report - " & Format(CDate(frmReportParameters.dtpStartDate), "dd-mmm-yyyy") & " to " & Format(CDate(frmReportParameters.dtpEndDate), "dd-mmm-yyyy")
    Case 5 ' Single Product Report
      If Not ReportParameters(True, True) Then Exit Sub
      ReportBody = SingleProductReport(frmReportParameters.cmbProduct.ItemData(frmReportParameters.cmbProduct.ListIndex), frmReportParameters.cmbProduct.List(frmReportParameters.cmbProduct.ListIndex), CDate(frmReportParameters.dtpStartDate), CDate(frmReportParameters.dtpEndDate))
      ReportTitle = "Single Product Report - " & frmReportParameters.cmbProduct.List(frmReportParameters.cmbProduct.ListIndex) & " from " & Format(CDate(frmReportParameters.dtpStartDate), "dd-mmm-yyyy") & " to " & Format(CDate(frmReportParameters.dtpEndDate), "dd-mmm-yyyy")
    Case Else
      cmdGenerateReport.Enabled = False
      ReportTitle = ""
    End Select
  If ReportTitle <> "" Then
      frmPrintPreview.Caption = ReportTitle
      frmPrintPreview.rtbPrintPreview = ReportBody
      frmPrintPreview.Show 1
    End If
  txtUPC.SetFocus
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:cmdGenerateReport_Click", "") <> vbYes Then End
End Sub

' Generates a report showing activity for a single product over a time period
Private Function SingleProductReport(ByVal ProductCode As Integer, ByVal ProductName As String, ByVal StartDate As Date, ByVal EndDate As Date) As String
  Dim adoSortedTransfers As New ADODB.Recordset
  Dim Result As String
  Dim DayCount As Integer, DayBottles As Integer, DayDollars As Currency
  Dim GrandCount As Integer, GrandBottles As Integer, GrandDollars As Currency
  Dim PrintingDate As Long
  
  ' All reports are date ranges, not time ranges, so normalize the date to be the beginning of StartDate and the end of EndDate (actually the beginning of the following date)
  StartDate = CDate(Format(StartDate, "mm-dd-yyyy"))
  EndDate = CDate(Format(EndDate, "mm-dd-yyyy")) + 1
  
  ' First, open a recordset that has the transfers sorted by date
  Set adoSortedTransfers = New ADODB.Recordset
  On Error Resume Next
  Err.Clear
  adoSortedTransfers.Open "Select * From Transfers Where [DateTime] >= #" & Format(StartDate, "mm-dd-yyyy") & "# and [DateTime] <= #" & Format(EndDate + 1, "mm-dd-yyyy") & "# and [ProductCode] = " & CStr(ProductCode) & " Order By DateTime", adoTransfersDatabase, , , adCmdText
  If Err <> 0 Then
    SingleProductReport = BuildRTBString(2, "\b Error: unable to query transfers to build single product report!\plain\f2\fs17\par ")
    Exit Function
  End If
  
  ' Start with zeroes in everything
  DayCount = 0
  DayBottles = 0
  DayDollars = 0
  GrandCount = 0
  GrandBottles = 0
  GrandDollars = 0
  PrintingDate = 0
  Result = "\b Activity for " & ProductName & " during " & Format(StartDate, "dd-mmm-yyyy")
  If StartDate <> EndDate - 1 Then Result = Result & " to " & Format(EndDate - 1, "dd-mmm-yyyy")
  Result = Result & "\plain\f2\fs17\par\par "
  
  ' Loop through all transfers
  adoSortedTransfers.MoveFirst
  Do Until adoSortedTransfers.EOF
    ' Only pay attention to ones dated today
    If adoSortedTransfers!DateTime >= StartDate And adoSortedTransfers!DateTime <= EndDate Then
        If CLng(adoSortedTransfers!DateTime) <> PrintingDate Then
            ' this is a new date, time to dump and clear
            If PrintingDate <> 0 Then
                ' The check above prevents us dumping the nothing we had before we first came in.
                ' Print the conclusion line for a day
                Result = Result & "\b Day Totals for " & CStr(DayCount) & " transfers\tab " & CStr(DayBottles)
                Result = Result & "\tab\tab " & Format(DayDollars, "$#,##0.00") & "\plain\f2\fs17\par\par "
              End If
            ' This is the first item in a new date.
            ' Store the date for future use
            PrintingDate = CLng(adoSortedTransfers!DateTime)
            ' Display a date header
            Result = Result & "\b " & Format(CDate(PrintingDate), "dd-mmm-yyyy") & "\tab Qty\tab Price\tab Dollars\plain\f2\fs17\par "
            ' Tally the previous day into the grand total
            GrandCount = GrandCount + DayCount
            GrandBottles = GrandBottles + DayBottles
            GrandDollars = GrandDollars + DayDollars
            ' Start the counts for the new day
            DayCount = 1
            DayBottles = adoSortedTransfers!Quantity
            DayDollars = adoSortedTransfers!Quantity * adoSortedTransfers!Price
          Else
            ' This date is the same as the last one, so accumulate it.
            DayCount = DayCount + 1
            DayBottles = DayBottles + adoSortedTransfers!Quantity
            DayDollars = DayDollars + adoSortedTransfers!Price * adoSortedTransfers!Quantity
          End If
        ' print this one's line item
        Result = Result & "    " & Format(adoSortedTransfers!DateTime, "hh:mm:ssampm") & "\tab "
        Result = Result & CStr(adoSortedTransfers!Quantity) & "\tab "
        Result = Result & Format(adoSortedTransfers!Price, "$#,##0.00") & "\tab "
        Result = Result & Format(adoSortedTransfers!Quantity * adoSortedTransfers!Price, "$#,##0.00") & "\par "
      End If
    adoSortedTransfers.MoveNext
  Loop
  adoSortedTransfers.Close
  ' Now we need to dump the last one that never got dumped because we hit the end
  Result = Result & "\b Day Totals for " & CStr(DayCount) & " transfers\tab " & CStr(DayBottles) & "\tab\tab "
  Result = Result & Format(DayDollars, "$#,##0.00") & "\par\par "
  GrandCount = GrandCount + DayCount
  GrandBottles = GrandBottles + DayBottles
  GrandDollars = GrandDollars + DayDollars
  
  If GrandBottles <> 0 Then
      ' Add in a grand totals section
  Result = Result & "\b Grand Totals for " & CStr(GrandCount) & " transfers\tab " & CStr(GrandBottles) & "\tab\tab " & Format(GrandDollars, "$#,##0.00") & "\par "
    Else
      ' This can only happen if there wasn't anything at all!
      Result = "\b No transfers on file for selected product on selected date(s).\plain\f2\fs17\par "
    End If

  SingleProductReport = BuildRTBString(3, Result)
End Function

'-----------------------------------------------------------------------------
' Utility Functions ----------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub cmdBackup_Click()
  Dim s As String
  On Error GoTo ErrorHandler
  s = BrowseForFolder(frmMain, "Select where to back up your databases.", BackupLocation)
  If s <> "" Then
      If Not BackupDataFiles(s) Then
          MsgBox "Data backup not completed!", vbCritical
        End If
    End If
  txtUPC.SetFocus
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:cmdBackup_Click", "") <> vbYes Then End
End Sub

Private Function BackupDataFiles(ByVal s As String) As Boolean
  Dim fs As Object
  On Error GoTo ErrorHandler
  BackupLocation = s
  If Right$(BackupLocation, 1) <> "\" Then BackupLocation = BackupLocation & "\"
  frmMain.Caption = "Closing databases, please wait..."
  frmMain.MousePointer = vbHourglass
  ' close databases
  If Not CloseDatabases() Then
      Form_DefaultTitle
      frmMain.MousePointer = vbDefault
      BackupDataFiles = False
      Exit Function
    End If
  ' copy files
  frmMain.Caption = "Backing up databases, please wait..."
  Set fs = CreateObject("Scripting.FileSystemObject")
  fs.CopyFile "Products.mdb", BackupLocation, True
  fs.CopyFile "Transfers.mdb", BackupLocation, True
  ' open databases
  frmMain.Caption = "Opening databases, please wait..."
  If Not OpenDatabases() Then End
  Form_DefaultTitle
  frmMain.MousePointer = vbDefault
  BackupDataFiles = True
  Exit Function
ErrorHandler:
  If ReportRuntimeError("frmMain:BackupDataFiles", "") <> vbYes Then End
End Function

Private Sub cmdEndDay_Click()
  Dim i As Integer
  On Error GoTo ErrorHandler
  i = MsgBox("Generate the daily report?", vbQuestion + vbYesNoCancel)
  If i = vbCancel Then Exit Sub
  If i = vbYes Then
      frmPrintPreview.Caption = "End Of Day Report - " & Format(Now, "dd-mmm-yyyy")
      frmPrintPreview.rtbPrintPreview = TotalsByCode(Now, Now, "Daily Totals Report")
      frmPrintPreview.Show 1
    End If
  If Not BackupDataFiles(BackupLocation) Then Exit Sub
  Me.Tag = "unloading"
  End
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:cmdEndDay_Click", "") <> vbYes Then End
End Sub

Private Sub cmdAbout_Click()
  On Error GoTo ErrorHandler
  MsgBox conAppName & " (v" & CStr(App.Major) & "." & CStr(App.Minor) & "." & CStr(App.Revision) & ")" & vbCrLf & vbCrLf & "This system was coded by HunterGreen (hawthorn@sover.net) for afrikandesi for Rent-A-Coder job #319065." & vbCrLf & vbCrLf & "All rights to this program are as defined by the Rent-A-Coder contracts and policies (http://www.rentacoder.com/RentACoder/SoftwareCoders/SoftwareSellerLegal.asp).", vbInformation, conAppName & " (v" & CStr(App.Major) & "." & CStr(App.Minor) & "." & CStr(App.Revision) & ")"
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmMain:cmdAbout_Click", "") <> vbYes Then End
End Sub


