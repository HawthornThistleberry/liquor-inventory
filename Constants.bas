Attribute VB_Name = "Constants"
' Constants used throughout the program.
' I've also thrown a few common routines which fit nowhere else in here.

Public Const conAppName = "Bottle Inventory Management For Bar"

Public Const conProdCat_Liquor = 1
Public Const conProdCat_Beer = 2
Public Const conProdCat_Wine = 3
Public Const conProdCat_Other = 4

Public Const rtbPrefix1 = "{\rtf1\ansi\deff0\deftab720{\fonttbl{\f0\fswiss MS Sans Serif;}{\f1\froman\fcharset2 Symbol;}" & _
    "{\f2\fswiss MS Sans Serif;}}"
Public Const rtbPrefix2 = "{\colortbl\red0\green0\blue0;\red128\green128\blue128;\red192\green192\blue192;\red128\green0\blue0;\red255\green255\blue255;\red128\green128\blue0;\red255\green255\blue0;\red0\green128\blue0;\red255\green0\blue0;\red0\green128\blue128;\red0\green255\blue255;\red0\green0\blue128;\red0\green255\blue0;\red128\green0\blue128;\red255\green0\blue255;\red0\green0\blue255;}"
Public Const rtbPrefix3 = "\deflang1033\pard"
Public Const rtbPrefix2tabs = "\tx4000\tx5600\plain\f2\fs17 "
Public Const rtbPrefix3tabs = "\tx4000\tx5600\tx7200\plain\f2\fs17 "
Public Const rtbSuffix = "\par }"

Public Function CategoryName(ByVal Category As Integer) As String
  Select Case Category
    Case conProdCat_Liquor
      CategoryName = "Liquor"
    Case conProdCat_Beer
      CategoryName = "Beer"
    Case conProdCat_Wine
      CategoryName = "Wine"
    Case Else
      CategoryName = "Other"
    End Select
End Function

Public Function BuildRTBString(ByVal Tabs As Integer, ByVal s As String) As String
  Dim r As String
  r = rtbPrefix1 & Chr(13) & Chr(10) & rtbPrefix2 & Chr(13) & Chr(10) & rtbPrefix3
  If Tabs = 2 Then r = r & rtbPrefix2tabs Else r = r & rtbPrefix3tabs
  BuildRTBString = r & s & Chr(13) & Chr(10) & rtbSuffix
End Function

Public Function ReportRuntimeError(ByVal Where As String, ByVal Other As String)
  Dim Msg As String
  Msg = "Please write down the following runtime error information:" & vbCrLf & vbCrLf
  Msg = Msg & "Where: " & Where & vbCrLf
  If Other <> "" Then Msg = Msg & "  " & Other & vbCrLf
  Msg = Msg & "Error Code: " & Err & ": " & Err.Description & vbCrLf
  Msg = Msg & "Source: " & Err.Source & vbCrLf & vbCrLf
  Msg = Msg & "Please note what you were doing at the time this occurred to facilitate fixing the bug." & vbCrLf & vbCrLf
  Msg = Msg & "Would you like the program to attempt to continue?  (Depending on what caused this error, this may not work, or result in instability or data loss.)"
  ReportRuntimeError = MsgBox(Msg, vbCritical + vbYesNo, "Critical Error Detected")
End Function
