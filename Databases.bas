Attribute VB_Name = "Databases"
' Functions and variables related to database management

' ADO handles for Products database and associated tables
Public adoProductsDatabase As ADODB.Connection
Public adoProductsTable As ADODB.Recordset
Public adoUPCsTable As ADODB.Recordset

' Variable to track the next available product code for adding products
Public NextProductCode As Integer

' ADO handles for Transfers database and associated table
Public adoTransfersDatabase As ADODB.Connection
Public adoTransfersTable As ADODB.Recordset

' Structure used to retrieve information about a product
Public Type ProductInfo
  ProductCode As Integer
  Name As String
  Price As Currency
  Category As Integer
  PackName As String
  PackNumber As Integer
  DefaultByPack As Boolean
  DefaultQuantity As Integer
  Notes As String
End Type

' Structure used to retrieve information about a transfer
Public Type Transfer
  DateTime As Date
  ProductCode As Integer
  Price As Currency
  Quantity As Integer
  Notes As String
End Type

' Open all the databases the program needs, and update the next product code as well
Public Function OpenDatabases() As Boolean
  On Error Resume Next
  Err.Clear
  ' open the product database and tables
  Set adoProductsDatabase = New ADODB.Connection
  Set adoProductsTable = New ADODB.Recordset
  Set adoUPCsTable = New ADODB.Recordset
  adoProductsDatabase.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Products.mdb;Mode=ReadWrite;Persist Security Info=False"
  adoProductsDatabase.Open
  If Err <> 0 Then
    MsgBox "Error opening Products database! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Close other applications and try again; if that doesn't help, restart the computer.", vbCritical
    OpenDatabases = False
    Exit Function
  End If
  adoProductsTable.Open "Products", adoProductsDatabase
  If Err <> 0 Then
    MsgBox "Error opening Products table! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Close other applications and try again; if that doesn't help, restart the computer.", vbCritical
    OpenDatabases = False
    Exit Function
  End If
  adoUPCsTable.Open "UPCs", adoProductsDatabase
  If Err <> 0 Then
    MsgBox "Error opening UPCs table! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Close other applications and try again; if that doesn't help, restart the computer.", vbCritical
    OpenDatabases = False
    Exit Function
  End If
  ' Find the highest product code being used so far
  NextProductCode = 0
  adoProductsTable.MoveFirst
  Do Until adoProductsTable.EOF
    If adoProductsTable!ProductCode > NextProductCode Then NextProductCode = adoProductsTable!ProductCode
    adoProductsTable.MoveNext
  Loop
  NextProductCode = NextProductCode + 1

  ' open the transfers database and table
  Err.Clear
  Set adoTransfersDatabase = New ADODB.Connection
  Set adoTransfersTable = New ADODB.Recordset
  adoTransfersDatabase.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Transfers.mdb;Mode=ReadWrite;Persist Security Info=False"
  adoTransfersDatabase.Open
  If Err <> 0 Then
    MsgBox "Error opening Transfers database! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Close other applications and try again; if that doesn't help, restart the computer.", vbCritical
    OpenDatabases = False
    Exit Function
  End If
  adoTransfersTable.Open "Transfers", adoTransfersDatabase
  If Err <> 0 Then
    MsgBox "Error opening Transfers table! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Close other applications and try again; if that doesn't help, restart the computer.", vbCritical
    OpenDatabases = False
    Exit Function
  End If
  OpenDatabases = True
End Function

' Close databases in preparation of shutting down
Public Function CloseDatabases() As Boolean
  Dim i As Integer
  Dim retcode As Boolean
  
  On Error Resume Next
  Err.Clear
  retcode = True
  
  ' close the product database and tables
  adoProductsTable.Close
  If Err <> 0 Then
    MsgBox "Error closing Products table! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Restart the computer after this program closes.", vbCritical
    Err.Clear
    retcode = False
  End If
  adoUPCsTable.Close
  If Err <> 0 Then
    MsgBox "Error closing UPCs table! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Restart the computer after this program closes.", vbCritical
    Err.Clear
    retcode = False
  End If
  adoProductsDatabase.Close
  If Err <> 0 Then
    MsgBox "Error closing Products database! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Restart the computer after this program closes.", vbCritical
    Err.Clear
    retcode = False
  End If
  Set adoProductsTable = Nothing
  Set adoUPCsTable = Nothing
  Set adoProductsDatabase = Nothing

  ' close the transfers database and table
  adoTransfersTable.Close
  If Err <> 0 Then
    MsgBox "Error closing Transfers table! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Restart the computer after this program closes.", vbCritical
    Err.Clear
    retcode = False
  End If
  adoTransfersDatabase.Close
  If Err <> 0 Then
    MsgBox "Error closing Transfers database! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Restart the computer after this program closes.", vbCritical
    Err.Clear
    retcode = False
  End If
  Set adoTransfersTable = Nothing
  Set adoTransfersDatabase = Nothing
  CloseDatabases = retcode
End Function

' Look up a UPC code and return the associated product code.
Public Function UPCLookup(ByVal UPC As String) As Integer
  Dim adoUPCLookup As ADODB.Recordset
  Dim rUPC As String
  Set adoUPCLookup = New ADODB.Recordset
  On Error Resume Next
  Err.Clear
  adoUPCLookup.Open "Select * From UPCs Where UPC = '" & PrepareUPC(UPC) & "'", adoProductsDatabase, , , adCmdText
  If Err <> 0 Then
    MsgBox "Error searching for a UPC! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Close other applications and try again; if that doesn't help, restart the computer.", vbCritical
    UPCLookup = 0
    Exit Function
  End If
  ' set a default, then set the lookup results, so if we fall through that lookup due to an error, the value stays at the default
  rUPC = ""
  rUPC = adoUPCLookup("UPC")
  If rUPC = UPC Then
      UPCLookup = adoUPCLookup("ProductCode")
    Else
      UPCLookup = 0
    End If
  adoUPCLookup.Close
End Function

' Look up a product code and return the associated product information.
Public Function ProductLookup(ByVal ProductCode As Integer, ProdInfo As ProductInfo) As Boolean
  Dim adoProductLookup As ADODB.Recordset
  Dim rProductCode As Integer
  Set adoProductLookup = New ADODB.Recordset
  On Error Resume Next
  Err.Clear
  adoProductLookup.Open "Select * From Products Where ProductCode = " & Str(ProductCode), adoProductsDatabase, , , adCmdText
  If Err <> 0 Then
    MsgBox "Error searching for a product! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Close other applications and try again; if that doesn't help, restart the computer.", vbCritical
    ProductLookup = False
    Exit Function
  End If
  ' set a default, then set the lookup results, so if we fall through that lookup due to an error, the value stays at the default
  rProductCode = 0
  rProductCode = adoProductLookup("ProductCode")
  If rProductCode = ProductCode Then
      With ProdInfo
        .ProductCode = rProductCode
        .Name = adoProductLookup("Name")
        .Price = adoProductLookup("Price")
        .Category = adoProductLookup("Category")
        .PackName = adoProductLookup("PackName")
        .PackNumber = adoProductLookup("PackNumber")
        .DefaultByPack = adoProductLookup("DefaultByPack")
        .DefaultQuantity = adoProductLookup("DefaultQuantity")
        .Notes = adoProductLookup("Notes")
      End With
      ProductLookup = True
    Else
      ProductLookup = False
    End If
  adoProductLookup.Close
End Function

' Update a particular field for a particular product
Public Function UpdateProductField(ByVal ProductCode As Integer, SQLString As String) As Boolean
  Dim UpdateRecordSet As ADODB.Recordset
  On Error Resume Next
  Err.Clear
  ProductFieldUpdate = True
  Set UpdateRecordSet = adoProductsDatabase.Execute("Update Products Set " & SQLString & " where ProductCode = " & Str(ProductCode) & ";")
  If Err <> 0 And Err <> 3704 And Err <> 3021 Then
      MsgBox "Unable to update a product! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
      ProductFieldUpdate = False
    End If
  UpdateRecordSet.Close
End Function

' Create a UPC record linking to a particular product code
' Returns 0 on failure, -1 on success, or a product code if the UPC is already linked to an existing code
Public Function LinkUPCToProductCode(ByVal UPC As String, ByVal ProductCode As Integer) As Integer
  Dim NewRecordSet As ADODB.Recordset
  Dim NewRecordValues As String
  Dim ProdInfo As ProductInfo
  
  On Error Resume Next
  Err.Clear
  ' find out if the UPC already exists
  ProdCode = UPCLookup(UPC)
  If ProdCode <> 0 Then
      If ProductLookup(ProdCode, ProdInfo) Then
          MsgBox "UPC " & UPC & " is already associated with " & ProdInfo.Name, vbExclamation
          LinkUPCToProductCode = ProdCode
          Exit Function
        End If
      ' the UPC is in the database but pointing to a missing product code
      ' update the UPC record to point to the new product code
      Set NewRecordSet = adoProductsDatabase.Execute("Update UPCs Set ProductCode = " & Str(ProductCode) & " where UPC = '" & UPC & "';")
      If Err <> 0 And Err <> 3704 And Err <> 3021 Then
          LinkUPCToProductCode = 0
          Exit Function
        End If
      NewRecordSet.Close
      LinkUPCToProductCode = -1
    Else
      ' create a UPC record that points to the product
      NewRecordValues = "'" & UPC & "'," & Str(ProductCode)
      Set NewRecordSet = adoProductsDatabase.Execute("Insert Into UPCs (UPC,ProductCode) Values (" & NewRecordValues & ");")
      If Err <> 0 And Err <> 3704 And Err <> 3021 Then
          LinkUPCToProductCode = 0
          Exit Function
        End If
      NewRecordSet.Close
      LinkUPCToProductCode = -1
    End If
End Function

' Delete a UPC record
Public Function DeleteUPC(ByVal UPC As String) As Boolean
  Dim NewRecordSet As ADODB.Recordset
  
  On Error Resume Next
  Err.Clear
  ' find out if the UPC already exists
  Set NewRecordSet = adoProductsDatabase.Execute("Delete From UPCs Where UPC = '" & UPC & "';")
  If Err <> 0 And Err <> 3704 And Err <> 3021 Then
      DeleteUPC = False
      Exit Function
    End If
  NewRecordSet.Close
  DeleteUPC = True
End Function

' Create a Transfer record
Public Function CreateTransfer(Xfer As Transfer) As Boolean
  Dim NewRecordSet As ADODB.Recordset
  Dim NewRecordValues As String
  
  On Error Resume Next
  Err.Clear
  NewRecordValues = "#" & CStr(Xfer.DateTime) & "#,"
  NewRecordValues = NewRecordValues & CStr(Xfer.ProductCode) & ","
  NewRecordValues = NewRecordValues & CStr(Xfer.Price) & ","
  NewRecordValues = NewRecordValues & CStr(Xfer.Quantity) & ","
  NewRecordValues = NewRecordValues & "'" & Xfer.Notes & "'"
  Set NewRecordSet = adoTransfersDatabase.Execute("Insert Into Transfers ([DateTime],[ProductCode],[Price],[Quantity],[Notes]) Values (" & NewRecordValues & ");")
  If Err <> 0 And Err <> 3704 And Err <> 3021 Then
      CreateTransfer = False
      Exit Function
    End If
  NewRecordSet.Close
  CreateTransfer = True
End Function

' Look up a transfer
Public Function TransferLookup(ByVal DateTime As Date, Xfer As Transfer) As Boolean
  Dim adoTransferLookup As ADODB.Recordset
  Dim rDateTime As Date
  Set adoTransferLookup = New ADODB.Recordset
  On Error Resume Next
  Err.Clear
  adoTransferLookup.Open "Select * From Transfers Where DateTime = #" & CStr(DateTime) & "#", adoTransfersDatabase, , , adCmdText
  If Err <> 0 Then
    MsgBox "Error searching for a transfer! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    MsgBox "Close other applications and try again; if that doesn't help, restart the computer.", vbCritical
    TransferLookup = False
    Exit Function
  End If
  ' set a default, then set the lookup results, so if we fall through that lookup due to an error, the value stays at the default
  rDateTime = 0
  rDateTime = adoTransferLookup("DateTime")
  If rDateTime = DateTime Then
      With Xfer
        .DateTime = rDateTime
        .ProductCode = adoTransferLookup("ProductCode")
        .Price = adoTransferLookup("Price")
        .Quantity = adoTransferLookup("Quantity")
        .Notes = adoTransferLookup("Notes")
      End With
      TransferLookup = True
    Else
      TransferLookup = False
    End If
  adoTransferLookup.Close
End Function

' Update an entire transfer record
Public Function UpdateTransfer(Xfer As Transfer) As Boolean
  Dim UpdateRecordSet As ADODB.Recordset
  Dim SQLString As String
  On Error Resume Next
  Err.Clear
  UpdateTransfer = True
  SQLString = "ProductCode = " & CStr(Xfer.ProductCode)
  SQLString = SQLString & ", Price = " & CStr(Xfer.Price)
  SQLString = SQLString & ", Quantity = " & CStr(Xfer.Quantity)
  SQLString = SQLString & ", Notes = '" & Xfer.Notes & "'"
  Set UpdateRecordSet = adoTransfersDatabase.Execute("Update Transfers Set " & SQLString & " Where DateTime = #" & CStr(Xfer.DateTime) & "#;")
  If Err <> 0 And Err <> 3704 And Err <> 3021 Then
      MsgBox "Unable to update a transfer! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
      UpdateTransfer = False
    End If
  UpdateRecordSet.Close
End Function


