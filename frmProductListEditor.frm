VERSION 5.00
Begin VB.Form frmProductListEditor 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Product List Editor"
   ClientHeight    =   7710
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10095
   Icon            =   "frmProductListEditor.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7710
   ScaleWidth      =   10095
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.Frame fraProductDetails 
      Caption         =   "Product Details"
      Height          =   7695
      Left            =   2400
      TabIndex        =   16
      Top             =   0
      Width           =   7695
      Begin VB.CommandButton cmdDeleteProduct 
         Caption         =   "Delete Product"
         Height          =   375
         Left            =   2640
         TabIndex        =   11
         Top             =   7200
         Visible         =   0   'False
         Width           =   2415
      End
      Begin VB.CommandButton cmdApplyChanges 
         Caption         =   "Apply Changes"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   10
         Top             =   7200
         Width           =   2415
      End
      Begin VB.Frame fraUPCs 
         Caption         =   "UPCs"
         Height          =   4335
         Left            =   5160
         TabIndex        =   28
         Top             =   3240
         Width           =   2415
         Begin VB.CommandButton cmdUPCsDelete 
            Caption         =   "Delete"
            Height          =   375
            Left            =   1200
            TabIndex        =   14
            Top             =   3840
            Width           =   1095
         End
         Begin VB.CommandButton cmdUPCsAdd 
            Caption         =   "Add"
            Height          =   375
            Left            =   120
            TabIndex        =   13
            Top             =   3840
            Width           =   1095
         End
         Begin VB.ListBox lisUPCs 
            Height          =   3570
            Left            =   120
            Sorted          =   -1  'True
            TabIndex        =   12
            Top             =   240
            Width           =   2175
         End
      End
      Begin VB.Frame fraNotes 
         Caption         =   "Notes"
         Height          =   3855
         Left            =   120
         TabIndex        =   27
         Top             =   3240
         Width           =   4935
         Begin VB.TextBox txtNotes 
            Height          =   3495
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   9
            Top             =   240
            Width           =   4695
         End
      End
      Begin VB.TextBox txtDefaultQuantity 
         Height          =   285
         Left            =   2280
         MaxLength       =   4
         TabIndex        =   5
         Text            =   "1"
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Frame fraPacks 
         Caption         =   "Product Packs"
         Height          =   1455
         Left            =   120
         TabIndex        =   21
         Top             =   1680
         Width           =   7455
         Begin VB.CheckBox chkDefaultUsePacks 
            Caption         =   "Use packs for transfers by default"
            Height          =   255
            Left            =   120
            TabIndex        =   8
            Top             =   1080
            Value           =   1  'Checked
            Width           =   3375
         End
         Begin VB.TextBox txtPackNumber 
            Height          =   285
            Left            =   840
            MaxLength       =   4
            TabIndex        =   7
            Text            =   "6"
            Top             =   720
            Width           =   2655
         End
         Begin VB.ComboBox cmbPackName 
            Height          =   315
            ItemData        =   "frmProductListEditor.frx":0442
            Left            =   840
            List            =   "frmProductListEditor.frx":045F
            TabIndex        =   6
            Text            =   "six-pack"
            Top             =   240
            Width           =   2655
         End
         Begin VB.Label lblPackExplanation 
            Caption         =   $"frmProductListEditor.frx":04A4
            Height          =   975
            Left            =   3600
            TabIndex        =   24
            Top             =   240
            Width           =   3735
         End
         Begin VB.Label lblPackNumber 
            Alignment       =   1  'Right Justify
            Caption         =   "Number"
            Height          =   255
            Left            =   120
            TabIndex        =   23
            Top             =   750
            Width           =   615
         End
         Begin VB.Label lblPackName 
            Alignment       =   1  'Right Justify
            Caption         =   "Name"
            Height          =   255
            Left            =   120
            TabIndex        =   22
            Top             =   270
            Width           =   615
         End
      End
      Begin VB.TextBox txtPrice 
         Height          =   285
         Left            =   1200
         MaxLength       =   10
         TabIndex        =   4
         Top             =   960
         Width           =   6375
      End
      Begin VB.TextBox txtName 
         Height          =   285
         Left            =   1200
         TabIndex        =   3
         Top             =   240
         Width           =   6375
      End
      Begin VB.Label lblDefaultQtyExplanation 
         Caption         =   "six-pack(s)"
         Height          =   255
         Left            =   3720
         TabIndex        =   26
         Top             =   1350
         Width           =   3855
      End
      Begin VB.Label lblDefaultQuantity 
         Caption         =   "Default Quantity Transferred:"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   1350
         Width           =   2055
      End
      Begin VB.Label lblPrice 
         Alignment       =   1  'Right Justify
         Caption         =   "Price $"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   1005
         Width           =   975
      End
      Begin VB.Label lblProductCode 
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1200
         TabIndex        =   19
         Top             =   600
         Width           =   6375
      End
      Begin VB.Label lblProductNum 
         Alignment       =   1  'Right Justify
         Caption         =   "Internal Code"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   645
         Width           =   975
      End
      Begin VB.Label lblDescription 
         Alignment       =   1  'Right Justify
         Caption         =   "Name"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   270
         Width           =   975
      End
   End
   Begin VB.Frame fraProductList 
      Caption         =   "Select Product"
      Height          =   7695
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   2295
      Begin VB.CommandButton cmdAdd 
         Caption         =   "Add New Product"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   7200
         Width           =   2055
      End
      Begin VB.ListBox lisProducts 
         Height          =   6495
         Left            =   120
         Sorted          =   -1  'True
         TabIndex        =   1
         Top             =   600
         Width           =   2055
      End
      Begin VB.ComboBox cmbCategory 
         Height          =   315
         ItemData        =   "frmProductListEditor.frx":0599
         Left            =   120
         List            =   "frmProductListEditor.frx":05A9
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   240
         Width           =   2055
      End
   End
End
Attribute VB_Name = "frmProductListEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Product List Editor
' Written by Frank J. Perricone
' for Rent-A-Coder job #319065

' This form is used to edit the product list.

'-----------------------------------------------------------------------------
' Form Handling --------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub Form_Load()
  On Error Resume Next
  ' position the window where it was last, or if it was never positioned before, in the middle
  Left = GetSetting(conAppName, "ProductListEditor", "Left", (Screen.Width - Width) / 2)
  Top = GetSetting(conAppName, "ProductListEditor", "Top", (Screen.Height - Height) / 2)
  cmbCategory.ListIndex = GetSetting(conAppName, "ProductListEditor", "LastCategory", 1) - 1
End Sub

Private Sub Form_Activate()
  On Error GoTo ErrorHandler
  If cmbCategory.ListIndex < 0 Then cmbCategory.ListIndex = 0
  If Me.Tag <> "" Then ' then we're here just to create a particular product
      ' Me.Tag will hold a category number, a space, and then a UPC
      cmbCategory.ListIndex = Val(Word(Me.Tag, 1)) - 1
      If Not CreateNewProduct(Word(Me.Tag, 2), cmbCategory.ListIndex + 1) Then
          Me.Hide
        End If
    Else
      cmbCategory_Click
    End If
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:Form_Unload", "") <> vbYes Then End
End Sub

Private Sub Form_Unload(Cancel As Integer)
  ' just in case something wasn't saved, save all fields
  On Error Resume Next
  If Me.Visible Then
      If Me.ActiveControl = txtName Then txtName_LostFocus
      If Me.ActiveControl = txtPrice Then txtPrice_LostFocus
      If Me.ActiveControl = txtDefaultQuantity Then txtDefaultQuantity_LostFocus
      If Me.ActiveControl = cmbPackName Then cmbPackName_LostFocus
      If Me.ActiveControl = txtPackNumber Then txtPackNumber_LostFocus
      If Me.ActiveControl = chkDefaultUsePacks Then chkDefaultUsePacks_Click
      If Me.ActiveControl = txtNotes Then txtNotes_LostFocus
    End If
  ' save startup settings
  Call SaveSetting(conAppName, "ProductListEditor", "Left", Left)
  Call SaveSetting(conAppName, "ProductListEditor", "Top", Top)
  Call SaveSetting(conAppName, "ProductListEditor", "LastCategory", cmbCategory.ListIndex + 1)
End Sub

'-----------------------------------------------------------------------------
' Product List ---------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub cmbCategory_Click()
  If cmbCategory.ListIndex + 1 = conProdCat_Liquor Then
    ' Update the form to hide fields that do not apply to liquor
    ' (because liquor is always in single bottles, never in packs)
    fraPacks.Visible = False
    lblDefaultQtyExplanation = "bottle(s)"
  Else
    ' Display the fields hidden from Liquor
    fraPacks.Visible = True
  End If
  ' populate product list, storing product codes in the ItemData and names in the List
  lisProducts.Clear
  On Error Resume Next
  adoProductsTable.MoveFirst
  Do Until adoProductsTable.EOF
    If adoProductsTable!Category = cmbCategory.ListIndex + 1 Then
      lisProducts.AddItem adoProductsTable!Name
      lisProducts.ItemData(lisProducts.NewIndex) = adoProductsTable!ProductCode
    End If
    adoProductsTable.MoveNext
  Loop
  If lisProducts.ListCount > 0 Then
      ' move focus to product list
      lisProducts.SetFocus
      lisProducts.ListIndex = 0
      lisProducts_Click
    Else
      ' move focus to add button
      cmdAdd.SetFocus
    End If
End Sub

Private Sub lisProducts_Click()
  Dim ProdInfo As ProductInfo
  On Error GoTo ErrorHandler
  If Not ProductLookup(lisProducts.ItemData(lisProducts.ListIndex), ProdInfo) Then
      MsgBox "Unable to load data for product " & lisProducts.List(lisProducts.ListIndex) & "!", vbExclamation
      ' disable fields
      txtName.Enabled = False
      txtPrice.Enabled = False
      txtDefaultQuantity.Enabled = False
      cmbPackName.Enabled = False
      txtPackNumber.Enabled = False
      chkDefaultUsePacks.Enabled = False
      txtNotes.Enabled = False
      lisUPCs.Enabled = False
      cmdUPCsAdd.Enabled = False
      cmdUPCsDelete.Enabled = False
      Exit Sub
    End If
  ' enable fields
  txtName.Enabled = True
  txtPrice.Enabled = True
  txtDefaultQuantity.Enabled = True
  cmbPackName.Enabled = True
  txtPackNumber.Enabled = True
  chkDefaultUsePacks.Enabled = True
  txtNotes.Enabled = True
  lisUPCs.Enabled = True
  cmdUPCsAdd.Enabled = True
  cmdUPCsDelete.Enabled = True
  ' load the fields
  With ProdInfo
    txtName = .Name
    lblProductCode = .ProductCode
    txtPrice = Format(.Price, "#,##0.00")
    txtDefaultQuantity = .DefaultQuantity
    cmbPackName = .PackName
    cmbPackName_Change
    txtPackNumber = CStr(.PackNumber)
    If .DefaultByPack Then chkDefaultUsePacks = 1 Else chkDefaultUsePacks = 0
    txtNotes = .Notes
  End With
  ' load UPCs
  lisUPCs.Clear
  On Error Resume Next
  adoUPCsTable.MoveFirst
  Do Until adoUPCsTable.EOF
    If adoUPCsTable!ProductCode = ProdInfo.ProductCode Then
      lisUPCs.AddItem adoUPCsTable!UPC
    End If
    adoUPCsTable.MoveNext
  Loop
  On Error GoTo ErrorHandler
  If lisUPCs.ListCount > 0 Then
      ' enable delete
      cmdUPCsDelete.Enabled = True
      lisUPCs.ListIndex = 0
    Else
      ' disable add button
      cmdUPCsDelete.Enabled = False
    End If
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:lisProducts_Click", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Adding Products ------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub cmdAdd_Click()
  Dim UPC As String
  On Error GoTo ErrorHandler
  ' prompt for a UPC, with a cancel option; if the UPC is cancelled, abort
  UPC = InputBox("Scan or type the UPC of the new product.", , "")
  ' remove non-numeric characters; trim to 16 digits
  UPC = PrepareUPC(UPC)
  If UPC = "" Or UPC = "0000000000000000" Then Exit Sub
  ' pass the UPC to the main "new product" function
  CreateNewProduct UPC, cmbCategory.ListIndex + 1
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:cmdAdd_Click", "") <> vbYes Then End
End Sub

' The heart of the functionality is in a separate function so we can come in from
' the Transfers data entry form when we find a UPC isn't listed.
Public Function CreateNewProduct(UPC As String, Category As Integer) As Boolean
  Dim NewRecordSet As ADODB.Recordset
  Dim NewRecordValues As String, s As String
  Dim ProdInfo As ProductInfo
  Dim i As Integer, ProdCode As Integer
  
  On Error Resume Next
  
  ' create the UPC linked to the new product code
  ProdCode = LinkUPCToProductCode(UPC, NextProductCode)
  If ProdCode = 0 Then
      If MsgBox("Unable to update a UPC! (Error" & Str(Err) & " - " & Error$(Err) & ")" & vbCrLf & "Continue anyway?", vbYesNo + vbQuestion) <> vbYes Then
          CreateNewProduct = False
          Exit Function
        End If
    End If
  If ProdCode <> -1 And ProductLookup(ProdCode, ProdInfo) Then
      MsgBox "UPC " & UPC & " is already associated with " & ProdInfo.Name, vbExclamation
      ' if it's in a different category, switch to that category
      If ProdInfo.Category <> cmbCategory.ListIndex + 1 Then
          cmbCategory.ListIndex = ProdInfo.Category - 1
          cmbCategory_Click
        End If
      ' select it in the form
      For i = 0 To lisProducts.ListCount - 1
        If lisProducts.ItemData(i) = ProdCode Then
            lisProducts.ListIndex = i
            Exit Function
          End If
        Next
      MsgBox "Internal error finding the product in the list!", vbCritical
      Exit Function
    End If
  
  ' create a new Products record
  NewRecordValues = Str(NextProductCode) & ","
  Select Case cmbCategory.ListIndex + 1
    Case conProdCat_Liquor
      NewRecordValues = NewRecordValues & "'Unnamed Liquor',0,1,'',1,False,"
      s = "Unnamed Liquor"
    Case conProdCat_Beer
      NewRecordValues = NewRecordValues & "'Unnamed Beer',0,2,'six-pack',6,True,"
      s = "Unnamed Beer"
    Case conProdCat_Wine
      NewRecordValues = NewRecordValues & "'Unnamed Wine',0,3,'four-pack',4,True,"
      s = "Unnamed Wine"
    Case Else
      NewRecordValues = NewRecordValues & "'Unnamed Product',0,4,'',1,False,"
      s = "Unnamed Product"
    End Select
  NewRecordValues = NewRecordValues & "1,''"
  Set NewRecordSet = adoProductsDatabase.Execute("Insert Into Products (ProductCode,Name,Price,Category,PackName,PackNumber,DefaultByPack,DefaultQuantity,Notes) Values (" & NewRecordValues & ");")
  If Err <> 0 And Err <> 3704 And Err <> 3021 Then
    MsgBox "Unable to create a product! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbCritical
    CreateNewProduct = False
    Exit Function
  End If
  NewRecordSet.Close

  ' add it to the list
  lisProducts.AddItem s
  lisProducts.ItemData(lisProducts.NewIndex) = NextProductCode
  ' increment the next product code for the next time
  NextProductCode = NextProductCode + 1
  ' select tne new record in the list
  lisProducts.ListIndex = lisProducts.NewIndex
  
  ' position the cursor on the name field, which should be selected
  txtName.SetFocus
  CreateNewProduct = True
End Function

'-----------------------------------------------------------------------------
' Data Fields ----------------------------------------------------------------
'-----------------------------------------------------------------------------
' * Name *
Private Sub txtName_GotFocus()
  On Error GoTo ErrorHandler
  SelectField txtName
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtName_GotFocus", "") <> vbYes Then End
End Sub

Private Sub txtName_KeyPress(KeyAscii As Integer)
  On Error GoTo ErrorHandler
  KeyAscii = KP_NoQuotesAtAll(KeyAscii)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtName_KeyPress", "") <> vbYes Then End
End Sub

Private Sub txtName_LostFocus()
  On Error GoTo ErrorHandler
  UpdateProductField Val(lblProductCode), "Name = '" & txtName & "'"
  lisProducts.List(lisProducts.ListIndex) = txtName
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtName_LostFocus", "") <> vbYes Then End
End Sub

' * Price *
Private Sub txtPrice_GotFocus()
  On Error GoTo ErrorHandler
  SelectField txtPrice
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtPrice_GotFocus", "") <> vbYes Then End
End Sub

Private Sub txtPrice_KeyPress(KeyAscii As Integer)
  On Error GoTo ErrorHandler
  KeyAscii = KP_OnlyDigitsAndPeriod(KeyAscii)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtPrice_KeyPress", "") <> vbYes Then End
End Sub

Private Sub txtPrice_LostFocus()
  On Error GoTo ErrorHandler
  txtPrice = Format(txtPrice, "#,##0.00")
  UpdateProductField Val(lblProductCode), "Price = " & CCur(txtPrice)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtPrice_LostFocus", "") <> vbYes Then End
End Sub

' * DefaultQuantity *
Private Sub txtDefaultQuantity_GotFocus()
  On Error GoTo ErrorHandler
  SelectField txtDefaultQuantity
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtDefaultQuantity_GotFocus", "") <> vbYes Then End
End Sub

Private Sub txtDefaultQuantity_KeyPress(KeyAscii As Integer)
  On Error GoTo ErrorHandler
  KeyAscii = KP_OnlyDigits(KeyAscii)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtDefaultQuantity_KeyPress", "") <> vbYes Then End
End Sub

Private Sub txtDefaultQuantity_LostFocus()
  On Error GoTo ErrorHandler
  UpdateProductField Val(lblProductCode), "DefaultQuantity = " & CStr(txtDefaultQuantity)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtDefaultQuantity_LostFocus", "") <> vbYes Then End
End Sub

' * PackName *
Private Sub cmbPackName_GotFocus()
  On Error GoTo ErrorHandler
  SelectField cmbPackName
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:cmbPackName_GotFocus", "") <> vbYes Then End
End Sub

Private Sub cmbPackName_KeyPress(KeyAscii As Integer)
  On Error GoTo ErrorHandler
  KeyAscii = KP_NoQuotesAtAll(KeyAscii)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:cmbPackName_KeyPress", "") <> vbYes Then End
End Sub

Private Sub cmbPackName_Change()
  On Error GoTo ErrorHandler
  If cmbPackName = "" Then
      If cmbCategory.ListIndex + 1 = conProdCat_Other Then lblDefaultQtyExplanation = "unit(s)" Else lblDefaultQtyExplanation = "bottle(s)"
    Else
      lblDefaultQtyExplanation = cmbPackName & "(s)"
    End If
  If cmbPackName.ListIndex >= 0 Then
      txtPackNumber = cmbPackName.ItemData(cmbPackName.ListIndex)
      UpdateProductField Val(lblProductCode), "PackNumber = " & txtPackNumber
    End If
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:cmbPackName_Change", "") <> vbYes Then End
End Sub

Private Sub cmbPackName_Click()
  On Error GoTo ErrorHandler
  cmbPackName_Change
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:cmbPackName_Click", "") <> vbYes Then End
End Sub

Private Sub cmbPackName_LostFocus()
  On Error GoTo ErrorHandler
  UpdateProductField Val(lblProductCode), "PackName = '" & cmbPackName & "'"
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:cmbPackName_LostFocus", "") <> vbYes Then End
End Sub

' * PackNumber *
Private Sub txtPackNumber_GotFocus()
  On Error GoTo ErrorHandler
  SelectField txtPackNumber
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtPackNumber_GotFocus", "") <> vbYes Then End
End Sub

Private Sub txtPackNumber_KeyPress(KeyAscii As Integer)
  On Error GoTo ErrorHandler
  KeyAscii = KP_OnlyDigits(KeyAscii)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtPackNumber_KeyPress", "") <> vbYes Then End
End Sub

Private Sub txtPackNumber_LostFocus()
  On Error GoTo ErrorHandler
  UpdateProductField Val(lblProductCode), "PackNumber = " & txtPackNumber
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtPackNumber_LostFocus", "") <> vbYes Then End
End Sub

' * DefaultByPack
Private Sub chkDefaultUsePacks_Click()
  On Error GoTo ErrorHandler
  UpdateProductField Val(lblProductCode), "DefaultByPack = " & CStr(chkDefaultUsePacks = 1)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:chkDefaultUsePacks_Click", "") <> vbYes Then End
End Sub

' * Notes *
Private Sub txtNotes_KeyPress(KeyAscii As Integer)
  On Error GoTo ErrorHandler
  KeyAscii = KP_NoQuotesAtAll(KeyAscii)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtNotes_KeyPress", "") <> vbYes Then End
End Sub

Private Sub txtNotes_LostFocus()
  On Error GoTo ErrorHandler
  UpdateProductField Val(lblProductCode), "Notes = '" & txtNotes & "'"
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:txtNotes_LostFocus", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' UPCs Handling --------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub cmdUPCsAdd_Click()
  Dim UPC As String
  Dim ProdCode As Integer
  On Error GoTo ErrorHandler
  ' prompt for a UPC, with a cancel option; if the UPC is cancelled, abort
  UPC = InputBox("Scan or type the UPC of the new product.", , "")
  ' remove non-numeric characters; trim to 16 digits
  UPC = PrepareUPC(UPC)
  If UPC = "" Or UPC = "0000000000000000" Then Exit Sub
  ProdCode = LinkUPCToProductCode(UPC, Val(lblProductCode))
  If ProdCode = -1 Then
      lisUPCs.AddItem UPC
      lisUPCs.Refresh
      Exit Sub
    End If
  If ProdCode = 0 Then
      MsgBox "Unable to create UPC!", vbExclamation
      Exit Sub
    End If
  MsgBox "UPC " & UPC & " is already linked to another product.", vbInformation
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:cmdUPCsAdd_Click", "") <> vbYes Then End
End Sub

Private Sub cmdUPCsDelete_Click()
  Dim i As Integer
  On Error GoTo ErrorHandler
  If Not DeleteUPC(lisUPCs.List(lisUPCs.ListIndex)) Then
      MsgBox "Unable to delete UPC! (Error" & Str(Err) & " - " & Error$(Err) & ")", vbExclamation
    Else
      i = lisUPCs.ListIndex
      lisUPCs.RemoveItem lisUPCs.ListIndex
      If lisUPCs.ListCount = 0 Then
          cmdUPCsDelete.Enabled = False
        Else
          If i >= lisUPCs.ListCount Then i = lisUPCs.ListCount - 1
          lisUPCs.ListIndex = i
        End If
    End If
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmProductListEditor:cmdUPCsDelete_Click", "") <> vbYes Then End
End Sub


