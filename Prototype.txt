Attached is a picture that displays a prototype of what the program would look like.  Let me describe how it all works; read this while looking at the picture.

======================================================================
TRANSFER INVENTORY

In the upper left is the data entry area.  This is configured to make it as quick and efficient to do data entry as possible; the fields will automatically update to suit what you're doing.  The cursor always starts in the UPC box at the top, so you can scan or type a UPC code.  Once you do so, the cursor will jump to the quantity field.

If the item you scanned is beer or wine, where it says "packs" it'll change to whatever the type of pack that beer comes in (for instance, 12-packs).  Also, defaults will be filled in for the quantity (usually 1) and whether it's bottles or packs, all to make it as easy and quick as possible to enter data.  Most of the time you won't need to adjust any of this and just click Enter Transfer.

If the item is liquor, the choices for bottles and packs will vanish, because liquor is always transferred in bottles.  Again, a quantity will be defaulted in (usually 1, but that's up to you).

You'll set all these defaults when you edit the product list.  For instance, you might set Michelob Light as defaulting to being sold in packs, with a default quantity being 1, and the pack type being a 6-pack.  You might set another kind of beer to defaulting to being sold in cases.  If you usually send over three bottles of a particular kind of liquor at a time, you can set that as its default too.  Any of these defaults can be overridden when you actually enter the transfer, so this is just a way to save time.

======================================================================
DAILY TRANSFERS

The right side of the screen shows a summary of what's been going on that day.  It can display that information in either of two ways.

Daily Logs: This shows one line for each transfer you did that day, in order that they were entered.  You can double-click on one to edit it, in case something that you entered earlier in the day was entered incorrectly.  You can also add a note to to the entry at that point.  If you have transferred the same kind of product more than once in the same day, each transfer will be listed separately.  This display shows you the sequence of events, what happened at what time.

Totals By Code: This display is more like an ongoing report that automatically updates as the day goes on.  It will be grouped by type of product and show only one line for each product, even if it was transferred multiple times.  This is display only, and does not allow any editing.  It might look something like this:

    BEER
      Budweiser         6       $  3.84
      Bud Lite          3       $  1.92
    TOTAL               9       $  5.76

    LIQUOR
      Bacardi Silver    5       $ 63.75
      Crown Royal       1       $ 24.29
    TOTAL               6       $ 88.04

    GRAND TOTAL        15       $ 93.80

You can change between these two display types using the radio buttons at the bottom.

======================================================================
EDIT PRODUCT LIST

This will pop up a new window (not displayed here) which has a list of products and lets you add, edit, or delete them.  For each product, you will have the following things you can enter:

* description
* price per bottle/can
* category: liquor, beer, or wine
* pack size (not for liquor): name (e.g., "case") and quantity (e.g., 12)
* default to transferring in packs? (not for liquor)
* default quantity (usually 1)
* notes
* UPCs (up to 20 per product)

Note that if you should, in the Transfer Inventory box, scan a UPC that the system doesn't know about, you'll get a pop-up that asks you to choose one of these options:

* This is a new UPC for an existing product; link it to that product (with a list to choose a product)
* This is a new product (jumps you right into the product editor creating the new product)
* Cancel (lets you scan the UPC again in case it was entered incorrectly)

======================================================================
REPORTS

Select a report and then click Generate Report.  If necessary, you will be prompted for additional information, such as date ranges, before the report is generated.  The report will be displayed in a window first, and you can print it from there; this means you can also use reports just to do quick inquiries without printing them.

======================================================================
OTHER FUNCTIONS

Back Up Data: Will prompt you for a location to make a backup of your data file; it'll default to wherever you saved it last.

End Day: This does the Daily Totals report, then a backup, then closes the program.  You'll need to confirm that you really want to do this first.  Note that if you close the program by clicking the X in the upper right, it will prompt you if you want to do End Day or just close the program.

======================================================================
Note that whatever you do anywhere in the program, the cursor is always returned to the UPC box once you're done, so you're always ready to scan another item.
