VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmPrintPreview 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print Preview"
   ClientHeight    =   7725
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10170
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7725
   ScaleWidth      =   10170
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.CommandButton cmdCopy 
      Caption         =   "Copy to Clipboard"
      Height          =   375
      Left            =   0
      TabIndex        =   1
      Top             =   7320
      Width           =   1935
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6360
      TabIndex        =   2
      Top             =   7320
      Width           =   1815
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8280
      TabIndex        =   3
      Top             =   7320
      Width           =   1815
   End
   Begin RichTextLib.RichTextBox rtbPrintPreview 
      Height          =   7335
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10125
      _ExtentX        =   17859
      _ExtentY        =   12938
      _Version        =   393217
      ScrollBars      =   2
      RightMargin     =   10000
      TextRTF         =   $"frmPrintPreview.frx":0000
   End
   Begin MSComDlg.CommonDialog cdlGeneric 
      Left            =   0
      Top             =   7200
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmPrintPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Print Preview
' Written by Frank J. Perricone
' for Rent-A-Coder job #319065

' This form is used to display and print reports.  Before calling this, set
' the form title to the report title, and the rtbPrintPreview to the report.

'-----------------------------------------------------------------------------
' Form Handling --------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub Form_Load()
  On Error Resume Next
  ' position the window where it was last, or if it was never positioned before, in the middle
  Left = GetSetting(conAppName, "PrintPreview", "Left", (Screen.Width - Width) / 2)
  Top = GetSetting(conAppName, "PrintPreview", "Top", (Screen.Height - Height) / 2)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  On Error GoTo ErrorHandler
  ' save startup settings
  Call SaveSetting(conAppName, "PrintPreview", "Left", Left)
  Call SaveSetting(conAppName, "PrintPreview", "Top", Top)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmPrintPreview:Form_Unload", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Results Buttons ------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub cmdCopy_Click()
  On Error GoTo ErrorHandler
  Clipboard.Clear
  Clipboard.SetText rtbPrintPreview.Text, vbCFText
  Clipboard.SetText rtbPrintPreview.TextRTF, vbCFRTF
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmPrintPreview:cmdCopy_Click", "") <> vbYes Then End
End Sub

Private Sub cmdPrint_Click()
  Dim orig, body As String
  On Local Error GoTo printError
  cdlGeneric.CancelError = True
  cdlGeneric.Flags = cdlPDReturnDC + cdlPDNoPageNums
  If rtbPrintPreview.SelLength = 0 Then
      cdlGeneric.Flags = cdlGeneric.Flags + cdlPDAllPages
    Else
      cdlGeneric.Flags = cdlGeneric.Flags + cdlPDSelection
    End If
  cdlGeneric.ShowPrinter
  On Local Error Resume Next
  orig = rtbPrintPreview.TextRTF
  body = rtbPrintPreview.TextRTF
  ' change all the \fs17s to \fs24s in body
  i = InStr(body, "\fs17")
  Do While i <> 0
    body = Left$(body, i - 1) & "\fs24" & Mid$(body, i + 5)
    i = InStr(body, "\fs17")
    Loop
  Printer.Print ""
  rtbPrintPreview.TextRTF = body
  rtbPrintPreview.SelPrint cdlGeneric.hDC
  rtbPrintPreview.TextRTF = orig
  Printer.EndDoc
  Me.Tag = "print"
  Me.Hide
  Exit Sub
printError:
  If Err.Number <> cdlCancel Then
      MsgBox "Print Error: " & Err & "; " & Error, vbExclamation
    End If
End Sub

Private Sub cmdClose_Click()
  On Error GoTo ErrorHandler
  Me.Tag = "close"
  Me.Hide
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmPrintPreview:cmdClose_Click", "") <> vbYes Then End
End Sub



