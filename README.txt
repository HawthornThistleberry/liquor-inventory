My Program


This program will be used to keep track of inventory going from the Store to the Pub.

The following products are taken from the STore to the Pub:

Bottled Beer
Bottled Liquor
Wine bottles


Beer is counted per bottle. For example if a bartender takes 10 12pk's for beer that day,
she/he took 120 bottles for that day. Beer is taken in 12, 20, 24, and six packs. Each pack
has a barcode. Adding a beer product in the program should go like this:

BEER/WINE
=========

BARCODE: 4442523423 <-- SCAN # WITH HAND SCANNER
BRAND: BUDWEISER
COST PER BOTTLE: $0.80
QUANTITY: 12  <-- SAY BARCODE 4442523423 IS THE BARCODE OF A 12PK... YOU ENTER 12 BOTTLES

That is probably the most vital information needed when storing a beer product


LIQUOR
======

BARCODE: 42323423423 <-- AGAIN # WITH HAND SCANNER
BRAND: CRYSTAL PALACE
COST: $4.78 <-- REAL COST




PROGRAM FORM:
=============

ADD ENTRY: THIS BUTTON LEADS TO A SCREEN WHERE THE BARTENDER CAN SCAN THE BARCODE AND
	   ADD TO THE DAILY LIST OF STUFF SHE/HE TAKES OVER. FOR EXAMPLE:
	   WAYNE BRINGS OVER A 12PK OF BEER. HE CICKS 'ADD ENTRY' ON THE SOFTWARE AND
	   IN BARCODE BOX - HE TAKES HIS HAND BARCODE SCANNER AND SCANS 12K OF BEER. 
           THE PROGRAM WILL RECOGNIZE THE PRODUCT BECAUSE IT WILL ABREADY BE STORED IN
	   THE DATABASE. HENCE 12 BOTTLES OF BEER ARE ADDED TO THE DAILY COUNT.

ADD/EDIT PRODUCTS: HERE IS WHERE I WILL BE ABLE TO MANAGE THE DATABASE. THE INFO OF WHAT
                   NEEDS TO BE ADDED FOR EACH PRODUCT IS ABOVE

VIEW DAIY TOTALS: WHEN THIS BUTTON IS CLICKED - BASICALLY THE INFO I NEED TO SEE IS:

	   EXMAPLE:
	   
	   BEER/WINE                     TOTAL	
           BUDWEISTER                    120
	   MICHELOB LIGHT                72
           CORONA                        6
           TOTAL                         198
           ====================================== 

           LIQUOR                        TOTAL
           GOLDSCHLAGER                  1
           CRYSTAL PALACE VODKA          4
           TOTAL                         5


GENERATE CUSTOM REPORT:  BASICALY I NEED TO BE ABLE TO DO A DAY-TO-DATE / MONTH-TO-DATE 
                         AND YEAR-TO-DATE TOTAL ON THE INVENTORY TAKEN TO THE BAR. NEED
                         TOTALS. I ALSO WANT TO BE ABLE TO CREATE REPORTS ON A SPECIFIC 
                         PRODUCT. FOR EXAMPLE.. SAY I WANT TO SEE HOW MANY BOTTLES OF 
                         BUD LIGHT HAVE BEEN TAKEN IN THE MONTH OF JANUARY 2005. I WANT
                         TO BE ABLE TO GENERATE THAT REPORT. THE REPORT BASICALLY NEEDS 
                         TO HAVE THE COST AND QUANTITY OF EACH PRODUCT IN A FULL SHEET 
                         INCLUDING TOTALS. EVERY REPORT MUST BE ABLE TO PRINT ON A LETTER
                         SIZE PAPER.