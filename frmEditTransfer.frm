VERSION 5.00
Begin VB.Form frmEditTransfer 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Edit Transfer"
   ClientHeight    =   4110
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4110
   ScaleWidth      =   5910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3120
      TabIndex        =   13
      Top             =   3720
      Width           =   2775
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      TabIndex        =   12
      Top             =   3720
      Width           =   2895
   End
   Begin VB.Frame fraEditTransfer 
      Caption         =   "Edit Transfer"
      Height          =   3615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5895
      Begin VB.TextBox txtNotes 
         Height          =   1455
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   11
         Top             =   2040
         Width           =   5655
      End
      Begin VB.TextBox txtQuantity 
         Height          =   285
         Left            =   1320
         MaxLength       =   4
         TabIndex        =   8
         Top             =   1320
         Width           =   4455
      End
      Begin VB.TextBox txtPrice 
         Height          =   285
         Left            =   1320
         MaxLength       =   10
         TabIndex        =   6
         Top             =   960
         Width           =   4455
      End
      Begin VB.ComboBox cmbProduct 
         Height          =   315
         Left            =   1320
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   600
         Width           =   4455
      End
      Begin VB.Label lblDollarsDollarSign 
         Caption         =   "$"
         Height          =   255
         Left            =   1200
         TabIndex        =   15
         Top             =   1710
         Width           =   135
      End
      Begin VB.Label lblPriceDollarSign 
         Caption         =   "$"
         Height          =   255
         Left            =   1200
         TabIndex        =   14
         Top             =   990
         Width           =   135
      End
      Begin VB.Label lblTotalDollars 
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1320
         TabIndex        =   10
         Top             =   1680
         Width           =   4455
      End
      Begin VB.Label lblDollars 
         Alignment       =   1  'Right Justify
         Caption         =   "Dollars"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   1710
         Width           =   975
      End
      Begin VB.Label lblQuantity 
         Alignment       =   1  'Right Justify
         Caption         =   "Quantity"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1350
         Width           =   975
      End
      Begin VB.Label lblPrice 
         Alignment       =   1  'Right Justify
         Caption         =   "Price"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   990
         Width           =   975
      End
      Begin VB.Label lblProduct 
         Alignment       =   1  'Right Justify
         Caption         =   "Product"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   630
         Width           =   975
      End
      Begin VB.Label lblDateTime 
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1320
         TabIndex        =   2
         Top             =   240
         Width           =   4455
      End
      Begin VB.Label lblTimestamp 
         Alignment       =   1  'Right Justify
         Caption         =   "Date/Time"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   270
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmEditTransfer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Transfer Editor
' Written by Frank J. Perricone
' for Rent-A-Coder job #319065

' This form is used to allow the user to edit a transfer.
' This can only be done during the current day.

Public DateTime As Date

'-----------------------------------------------------------------------------
' Form Handling --------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub Form_Load()
  On Error Resume Next
  ' position the window where it was last, or if it was never positioned before, in the middle
  Left = GetSetting(conAppName, "EditTransfer", "Left", (Screen.Width - Width) / 2)
  Top = GetSetting(conAppName, "EditTransfer", "Top", (Screen.Height - Height) / 2)
End Sub

Private Sub Form_Activate()
  Dim Xfer As Transfer, i As Integer
  Me.MousePointer = vbHourglass
  ' load the product list
  cmbProduct.Clear
  On Error Resume Next
  adoProductsTable.MoveFirst
  Do Until adoProductsTable.EOF
    cmbProduct.AddItem CategoryName(adoProductsTable!Category) & ": " & adoProductsTable!Name
    cmbProduct.ItemData(cmbProduct.NewIndex) = adoProductsTable!ProductCode
    adoProductsTable.MoveNext
  Loop
  lblDateTime = Format(Me.DateTime, "mm-dd-yyyy hh:mm:ssampm")
  ' find the passed-in transfer
  If Not TransferLookup(Me.DateTime, Xfer) Then
      MsgBox "Unable to find the transfer to edit!", vbExclamation
      Me.Tag = "cancel"
      Me.Hide
    End If
  ' use them to load the fields
  txtPrice = Format(Xfer.Price, "#,##0.00")
  txtQuantity = CStr(Xfer.Quantity)
  lblTotalDollars_Update
  txtNotes = Xfer.Notes
  ' find the product
  For i = 0 To cmbProduct.ListCount - 1
    If cmbProduct.ItemData(i) = Xfer.ProductCode Then cmbProduct.ListIndex = i
    Next
  Me.MousePointer = vbDefault
End Sub

Private Sub Form_Unload(Cancel As Integer)
  On Error GoTo ErrorHandler
  ' save startup settings
  Call SaveSetting(conAppName, "EditTransfer", "Left", Left)
  Call SaveSetting(conAppName, "EditTransfer", "Top", Top)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmEditTransfer:Form_Unload", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Data Entry -----------------------------------------------------------------
'-----------------------------------------------------------------------------
' * Price *
Private Sub txtPrice_GotFocus()
  On Error GoTo ErrorHandler
  SelectField txtPrice
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmEditTransfer:txtPrice_GotFocus", "") <> vbYes Then End
End Sub

Private Sub txtPrice_KeyPress(KeyAscii As Integer)
  On Error GoTo ErrorHandler
  KeyAscii = KP_OnlyDigitsAndPeriod(KeyAscii)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmEditTransfer:txtPrice_KeyPress", "") <> vbYes Then End
End Sub

Private Sub txtPrice_LostFocus()
  On Error GoTo ErrorHandler
  txtPrice = Format(txtPrice, "#,##0.00")
  lblTotalDollars_Update
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmEditTransfer:txtPrice_LostFocus", "") <> vbYes Then End
End Sub

' * Quantity *
Private Sub txtQuantity_GotFocus()
  On Error GoTo ErrorHandler
  SelectField txtQuantity
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmEditTransfer:txtQuantity_GotFocus", "") <> vbYes Then End
End Sub

Private Sub txtQuantity_KeyPress(KeyAscii As Integer)
  On Error GoTo ErrorHandler
  KeyAscii = KP_OnlyDigits(KeyAscii)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmEditTransfer:txtQuantity_KeyPress", "") <> vbYes Then End
End Sub

Private Sub txtQuantity_LostFocus()
  On Error GoTo ErrorHandler
  lblTotalDollars_Update
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmEditTransfer:txtQuantity_LostFocus", "") <> vbYes Then End
End Sub

' * Notes *
Private Sub txtNotes_KeyPress(KeyAscii As Integer)
  On Error GoTo ErrorHandler
  KeyAscii = KP_NoQuotesAtAll(KeyAscii)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmEditTransfer:txtNotes_KeyPress", "") <> vbYes Then End
End Sub

Private Sub lblTotalDollars_Update()
  On Error GoTo ErrorHandler
  lblTotalDollars = Format(CCur(txtPrice) * CInt(txtQuantity), "#,##0.00")
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmEditTransfer:lblTotalDollars_Update", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Results Buttons ------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub cmdOK_Click()
  ' save the changes to the transfers database
  Dim Xfer As Transfer
  On Error GoTo ErrorHandler
  Xfer.DateTime = Me.DateTime
  Xfer.ProductCode = cmbProduct.ItemData(cmbProduct.ListIndex)
  Xfer.Price = CCur(txtPrice)
  Xfer.Quantity = CInt(txtQuantity)
  Xfer.Notes = txtNotes
  If Not UpdateTransfer(Xfer) Then
      MsgBox "Unable to save changes to transfer!", vbExclamation
      Me.Tag = "cancel"
    Else
      Me.Tag = "ok"
    End If
  Me.Hide
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmEditTransfer:cmdOK_Click", "") <> vbYes Then End
End Sub

Private Sub cmdCancel_Click()
  On Error GoTo ErrorHandler
  Me.Tag = "cancel"
  Me.Hide
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmEditTransfer:cmdCancel_Click", "") <> vbYes Then End
End Sub


