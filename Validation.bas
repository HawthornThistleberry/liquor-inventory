Attribute VB_Name = "Validation"
' Utility functions related to data entry and validation.

Public Sub SelectField(o As Object)
  o.SelStart = 0
  o.SelLength = Len(o)
End Sub

Public Function KP_NoQuotes(ByVal KeyAscii As Integer) As Integer
  KP_NoQuotes = KeyAscii
  If KeyAscii = 34 Then KP_NoQuotes = Asc("'")
End Function

Public Function KP_NoQuotesAtAll(ByVal KeyAscii As Integer) As Integer
  KP_NoQuotesAtAll = KeyAscii
  If KeyAscii = 34 Or KeyAscii = Asc("'") Then
      KP_NoQuotesAtAll = 0
      Beep
    End If
End Function

Public Function KP_OnlyDigits(ByVal KeyAscii As Integer) As Integer
  KP_OnlyDigits = KeyAscii
  If InStr("0123456789", Chr$(KeyAscii)) = 0 And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      KP_OnlyDigits = 0
      Beep
    End If
End Function

Public Function KP_OnlyDigitsAndMinus(ByVal KeyAscii As Integer) As Integer
  KP_OnlyDigitsAndMinus = KeyAscii
  If (Screen.ActiveControl.SelStart <> 0 Or (Chr$(KeyAscii) <> "-" And Chr$(KeyAscii) <> "+")) And _
      InStr("0123456789", Chr$(KeyAscii)) = 0 And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      KP_OnlyDigitsAndMinus = 0
      Beep
    End If
End Function

Public Function KP_OnlyDigitsAndPeriod(ByVal KeyAscii As Integer) As Integer
  KP_OnlyDigitsAndPeriod = KeyAscii
  If ((Chr$(KeyAscii) <> "." Or InStr(Screen.ActiveControl, ".") <> 0)) And _
      InStr("0123456789", Chr$(KeyAscii)) = 0 And _
      KeyAscii <> 8 And KeyAscii <> 127 Then
      KP_OnlyDigitsAndPeriod = 0
      Beep
    End If
End Function

Public Function Word(ByVal s As String, ByVal num As Integer) As String
  Dim i, j As Integer
  Dim s2 As String
  Word = ""
  i = 1
  For j = 1 To num - 1
    i = InStr(i, s, " ")
    If i = 0 Then Exit Function
    While i <= Len(s) And Mid$(s, i, 1) = " "
      i = i + 1
      Wend
    If i > Len(s) Then Exit Function
    Next j
  s2 = Mid$(s, i)
  j = InStr(s2, " ")
  If j <> 0 Then s2 = Left$(s2, j - 1)
  Word = s2
End Function

Public Function RemoveNonNumeric(ByVal s As String) As String
  Dim r As String
  Dim i As Integer
  Dim c As String
  r = ""
  For i = 1 To Len(s)
    c = Mid$(s, i, 1)
    If IsNumeric(c) Then r = r & c
    Next
  RemoveNonNumeric = r
End Function

Public Function LeftPad(ByVal s As String, ByVal c As String, ByVal l As Integer) As String
  LeftPad = Right$(String(l, c) & s, l)
End Function

Public Function PrepareUPC(ByVal s As String) As String
  PrepareUPC = LeftPad(RemoveNonNumeric(s), "0", 16)
End Function
