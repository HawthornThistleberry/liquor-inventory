VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmReportParameters 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Report Parameters"
   ClientHeight    =   1725
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4470
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1725
   ScaleWidth      =   4470
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2280
      TabIndex        =   4
      Top             =   1320
      Width           =   2175
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   1320
      Width           =   2175
   End
   Begin VB.Frame fraReportParameters 
      Caption         =   "Report Parameters"
      Height          =   1215
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   4455
      Begin VB.ComboBox cmbProduct 
         Height          =   315
         Left            =   840
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   720
         Width           =   3015
      End
      Begin MSComCtl2.DTPicker dtpEndDate 
         Height          =   255
         Left            =   2520
         TabIndex        =   1
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   24444929
         CurrentDate     =   38581
         MinDate         =   36892
      End
      Begin MSComCtl2.DTPicker dtpStartDate 
         Height          =   255
         Left            =   840
         TabIndex        =   0
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   24444929
         CurrentDate     =   38581
         MinDate         =   36892
      End
      Begin VB.Label lblProduct 
         Caption         =   "Product:"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   720
         Width           =   615
      End
      Begin VB.Label lblTo 
         Alignment       =   2  'Center
         Caption         =   "to"
         Height          =   255
         Left            =   2160
         TabIndex        =   7
         Top             =   360
         Width           =   375
      End
      Begin VB.Label lblDates 
         Caption         =   "Dates:"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   615
      End
   End
End
Attribute VB_Name = "frmReportParameters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Report Parameters
' Written by Frank J. Perricone
' for Rent-A-Coder job #319065

' This form is used to prompt the user for parameters for a report.
' Of the three input fields, only one is always displayed; the other two
' may be hidden beforehand by some reports.

'-----------------------------------------------------------------------------
' Form Handling --------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub Form_Load()
  On Error Resume Next
  ' position the window where it was last, or if it was never positioned before, in the middle
  Left = GetSetting(conAppName, "ReportParameters", "Left", (Screen.Width - Width) / 2)
  Top = GetSetting(conAppName, "ReportParameters", "Top", (Screen.Height - Height) / 2)
  dtpStartDate = Now
  dtpEndDate = Now
End Sub

Private Sub Form_Activate()
  If cmbProduct.Visible Then
      Me.MousePointer = vbHourglass
      ' load the product list
      cmbProduct.Clear
      On Error Resume Next
      adoProductsTable.MoveFirst
      Do Until adoProductsTable.EOF
        cmbProduct.AddItem CategoryName(adoProductsTable!Category) & ": " & adoProductsTable!Name
        cmbProduct.ItemData(cmbProduct.NewIndex) = adoProductsTable!ProductCode
        adoProductsTable.MoveNext
      Loop
      If cmbProduct.ListCount <= 0 Then
          MsgBox "Error: there are no products defined!  Edit the product list first.", vbExclamation
          Me.Tag = "cancel"
          Me.Hide
        End If
      cmbProduct.ListIndex = 0
      Me.MousePointer = vbDefault
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  On Error GoTo ErrorHandler
  ' save startup settings
  Call SaveSetting(conAppName, "ReportParameters", "Left", Left)
  Call SaveSetting(conAppName, "ReportParameters", "Top", Top)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmReportParameters:Form_Unload", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Results Buttons ------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub cmdOK_Click()
  On Error GoTo ErrorHandler
  Me.Tag = "ok"
  Me.Hide
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmReportParameters:cmdOK_Click", "") <> vbYes Then End
End Sub

Private Sub cmdCancel_Click()
  On Error GoTo ErrorHandler
  Me.Tag = "close"
  Me.Hide
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmReportParameters:cmdCancel_Click", "") <> vbYes Then End
End Sub
