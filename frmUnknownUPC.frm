VERSION 5.00
Begin VB.Form frmUnknownUPC 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Unknown UPC"
   ClientHeight    =   2475
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5580
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2475
   ScaleWidth      =   5580
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.ComboBox cmbCategory 
      Enabled         =   0   'False
      Height          =   315
      ItemData        =   "frmUnknownUPC.frx":0000
      Left            =   2640
      List            =   "frmUnknownUPC.frx":0010
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1320
      Width           =   2895
   End
   Begin VB.ComboBox cmbProduct 
      Height          =   315
      Left            =   2640
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   960
      Width           =   2895
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2880
      TabIndex        =   6
      Top             =   2040
      Width           =   2655
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   2040
      Width           =   2655
   End
   Begin VB.OptionButton optWhatToDo 
      Caption         =   "Oops!  Cancel this transfer."
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   4
      Top             =   1680
      Width           =   2415
   End
   Begin VB.OptionButton optWhatToDo 
      Caption         =   "Create a new product of type:"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   1320
      Width           =   2415
   End
   Begin VB.OptionButton optWhatToDo 
      Caption         =   "Link it to this product:"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Value           =   -1  'True
      Width           =   2415
   End
   Begin VB.Line linHorizontalSeparator 
      X1              =   120
      X2              =   5520
      Y1              =   840
      Y2              =   840
   End
   Begin VB.Label lblWhichIsNot 
      Alignment       =   2  'Center
      Caption         =   "which is not associated with any known product!"
      Height          =   255
      Left            =   600
      TabIndex        =   9
      Top             =   480
      Width           =   4935
   End
   Begin VB.Label lblUPC 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3000
      TabIndex        =   8
      Top             =   120
      Width           =   2535
   End
   Begin VB.Image imgUPCCode 
      Height          =   480
      Left            =   0
      Picture         =   "frmUnknownUPC.frx":002F
      Top             =   120
      Width           =   480
   End
   Begin VB.Label lblYouEnteredTheUPC 
      Alignment       =   2  'Center
      Caption         =   "You entered the UPC"
      Height          =   255
      Left            =   600
      TabIndex        =   7
      Top             =   135
      Width           =   2295
   End
End
Attribute VB_Name = "frmUnknownUPC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Unknown UPC Handler
' Written by Frank J. Perricone
' for Rent-A-Coder job #319065

' This form is used to ask the user how to handle an unknown UPC during data entry.

'-----------------------------------------------------------------------------
' Form Handling --------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub Form_Load()
  On Error Resume Next
  ' position the window where it was last, or if it was never positioned before, in the middle
  Left = GetSetting(conAppName, "UnknownUPC", "Left", (Screen.Width - Width) / 2)
  Top = GetSetting(conAppName, "UnknownUPC", "Top", (Screen.Height - Height) / 2)
  cmbCategory.ListIndex = GetSetting(conAppName, "UnknownUPC", "LastCategory", 1) - 1
End Sub

Private Sub Form_Activate()
  Me.MousePointer = vbHourglass
  If cmbCategory.ListIndex < 0 Then cmbCategory.ListIndex = 0
  ' load the product list
  cmbProduct.Clear
  On Error Resume Next
  adoProductsTable.MoveFirst
  Do Until adoProductsTable.EOF
    cmbProduct.AddItem CategoryName(adoProductsTable!Category) & ": " & adoProductsTable!Name
    cmbProduct.ItemData(cmbProduct.NewIndex) = adoProductsTable!ProductCode
    adoProductsTable.MoveNext
  Loop
  If cmbProduct.ListCount > 0 Then
      optWhatToDo(0).Enabled = True
      optWhatToDo(0) = True
      cmbProduct.ListIndex = 0
    Else
      optWhatToDo(0).Enabled = False
      optWhatToDo(1) = True
    End If
  Me.MousePointer = vbDefault
  Beep
End Sub

Private Sub Form_Unload(Cancel As Integer)
  On Error GoTo ErrorHandler
  ' save startup settings
  Call SaveSetting(conAppName, "UnknownUPC", "Left", Left)
  Call SaveSetting(conAppName, "UnknownUPC", "Top", Top)
  Call SaveSetting(conAppName, "UnknownUPC", "LastCategory", cmbCategory.ListIndex + 1)
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmUnknownUPC:Form_Unload", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Data Entry -----------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub optWhatToDo_Click(Index As Integer)
  On Error GoTo ErrorHandler
  If optWhatToDo(0) Then cmbProduct.Enabled = True Else cmbProduct.Enabled = False
  If optWhatToDo(1) Then cmbCategory.Enabled = True Else cmbCategory.Enabled = False
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmUnknownUPC:optWhatToDo_Click", "") <> vbYes Then End
End Sub

'-----------------------------------------------------------------------------
' Results Buttons ------------------------------------------------------------
'-----------------------------------------------------------------------------
Private Sub cmdOK_Click()
  On Error GoTo ErrorHandler
  If optWhatToDo(0) Then Me.Tag = "link " & CStr(cmbProduct.ItemData(cmbProduct.ListIndex))
  If optWhatToDo(1) Then Me.Tag = "create " & CStr(cmbCategory.ListIndex)
  If optWhatToDo(2) Then Me.Tag = "cancel"
  Me.Hide
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmUnknownUPC:cmdOK_Click", "") <> vbYes Then End
End Sub

Private Sub cmdCancel_Click()
  On Error GoTo ErrorHandler
  Me.Tag = "cancel"
  Me.Hide
  Exit Sub
ErrorHandler:
  If ReportRuntimeError("frmUnknownUPC:cmdCancel_Click", "") <> vbYes Then End
End Sub

